﻿class ExcelStatic
{
    ;XlDirection常量
    static xlDown := -4121    ;向下
    static xlToLeft := -4159  ;向左
    static xlToRight := -4161 ;向右
    static xlUp := -4162      ;向上
    ;XlAutoFillType常量
    static xlFillCopy:=1     ;将源区域的值和格式复制到目标区域
    static xlFillDefault:=0     ;确定用于填充目标区域的值和格式
    static xlFillFormats:=3     ;只复制格式
    static xlFillSeries:=2     ;扩展到目标区域，如1,2，扩展到3,4,5
    static xlFillValuse:=4     ;值
    static xlGrowthTrend:=10     ;乘法关系，如1,2，扩展到4,8,16
    static xlLinearTrend:=9     ;加法关系，如1,3，扩展到5,6,9
    ; XlLinestyle常量
    static xlDot:=-4118              ;
    static xlContinuous:=1              ;
    static xlDash:=-4115              ;
    static xlDouble:= -4119             ;
    ; XlBorderWeight常量
    static xlHairline:=1
    static xlMedium:=-4138
    static xlThick:=4
    static xlThin:=2
    ;XlBordersIndex    常量
    static xlDiagonalDown:=5
    static xlDiagonalUp:=6
    static xlEdgeBottom:=9
    static xlEdgeLeft:=7
    static xlEdgeRight:=10
    static xlEdgeTop:=8
    static xlInsideHorizontal:=12
    static xlInsideVertical:=11
    ;XlHAlign   常量
    static xlHAlignCenter	:=-4108
    static xlHAlignCenterAcrossSelection:=	7
    static xlHAlignDistributed	:=-4117
    static xlHAlignFill:=	5
    static xlHAlignGeneral:=	1
    static xlHAlignJustify:=	-4130
    static xlHAlignLeft:=	-4131
    static xlHAlignRight	:=-4152
    ;XlVAlign  常量
    static xlVAlignBottom:=-4107
    static xlVAlignCenter:=-4108
    static xlVAlignDistributed:=-4117
    static xlVAlignJustify:=-4130
    static xlVAlignTop:=-4160
    ; XlUnderlineStyle  常量
    static xlUnderlineStyleDouble:=-4119
    static xlUnderlineStyleDoubleAccounting:=5
    static xlUnderlineStyleNone:=-4142
    static xlUnderlineStyleSingle:=2
    static xlUnderlineStyleSingleAccounting:=4
    ; XlBuiltInDialog  常量
    static xlDialogPageSetup:=7
    static xlDialogSummaryInfo:=474
    static xlDialogFilter:=447
    static xlDialogSort:=39
    static xlDialogDataDelete:=36
    ; XlCVError  常量
    static xlErrDiv0:=2007
    static xlErrNA:=2042
    static xlErrName:=2029
    static xlErrNull:=2000
    static xlErrNum:=2036
    static xlErrRef:=2023
    static xlErrValue:=2015

    ; XlAutoFilterOperator  常量
    static 常量名:=对应数字
    static xlAnd:=1
    static xlBottom10Items:=4
    static xlBottom10Percent:=6
    static xlFilterCellColor:=8
    static xlFilterDynamic:=11
    static xlFilterFontColor:=9
    static xlFiterIcon:=10
    static xlFilterValues:=7
    static xlOr:=2
    static xlTop10Items:=3
    static xlTop10Percent:=5
    ; XlWindowView  常量
    static xlNormalView:=1
    static xlPageBreakPreview:=2
    ;自定义颜色 常量
    static CBlack:=1    ;黑
    static CWhite:=2    ;白
    static CRed:=3      ;红
    static CGreen:=4    ;绿
    static CBlue:=5     ;蓝
    static CYellow:=6       ;黄
    static CMagenta:=7       ;粉红
    static CCyan:=8       ;青
    static CGray25:=15       ;灰色25
    static CGray40:=48       ;灰色40
    static CGray50:=16       ;灰色50
    static CPurple:=17       ;紫
    static CLightRed:=22      ;浅红
    static CSkyBlue:=33       ;天蓝
    static CCyanGreen:=34       ;浅青绿
    static CLightGreen:=35       ;浅绿
    static CLightYellow:=36       ;浅黄
    static CLightBlue:=37       ;浅蓝
    static CRoseRed:=38       ;玫瑰红
    static CLigthPurple:=39       ;浅紫
    static COrangeBrown:=40       ;茶色
    static CMiddleBlue:=41       ;浅蓝
    static CWaterGreen:=42       ;水绿色
    static CGrassGreen:=43       ;青草绿
    static CGold:=44       ;金
    static CLightOrange:=45       ;浅橙
    static COrange:=46       ;橙
    static CBlueGray:=47       ;蓝灰
    static CDarkCyan:=49       ;深青
    static CMiddleGreen:=50       ;中绿
    static CTan:=53       ;褐
    static CDarkBlue:=55       ;黑蓝
    static CAuto:=-4105       ;自动
    static CNone:=-4142       ;无色
}