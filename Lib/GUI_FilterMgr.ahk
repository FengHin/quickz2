﻿GUI_FilterMgr_Load(Callback, CloseEvent:=""){
    Global FilterMgr, gQZConfig
    TBListID := IL_Create(10, 10, 0)
    IL_Add(TBListID, A_WinDir "\System32\imageres.dll", 68)
    IL_Add(TBListID, A_WinDir "\System32\mspaint.exe", 1)
    IL_Add(TBListID, A_WinDir "\System32\Shell32.dll", 248)
    IL_Add(TBListID, A_WinDir "\System32\Shell32.dll", 247)
    IL_Add(TBListID, A_WinDir "\System32\Shell32.dll", 160)
    
    FilterMgr := New GUI2("FilterMgr", "+LastFound +Theme -DPIScale")
    ;~ FilterMgr.SetFont(QZGlobal.FontSize, "s11 C800080 Bold Italic Microsoft YaHei")
    cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    FilterMgr.SetFont(cons_fontsize, "Microsoft YaHei")
    ;~ FilterMgr.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
    FilterMgr.AddCtrl("Text_ExistFilter", "Groupbox", "x10 y10 w380 h390", QZLang.TextExistFilter)
    ;~ FilterMgr.SetFont(QZGlobal.FontSize, "")
    FilterMgr.AddCtrl("TV_ExistFilter", "TreeView", "x20 y32 w230 h355 altsubmit -Lines +0x1000")
    FilterMgr.AddCtrl("TextFilterType", "Text", "x260 y35 w120 h25 ", QZLang.TextFilterType)    
    Types := ["All", "FileExt", "WinClass", "TextRegex", "keyword", "Function", "FileMulti"
             , "FileName", "FileDir", "WinExe", "WinTitle", "WinControl"]
    Name :=  ""
    Loop, % Types.MaxIndex()
        Name .= QZLang["Filter" Types[A_Index]] "|"
    FilterMgr.AddCtrl("DDL_Type" , "DropDownList", "x260 y60 w120 r12 choose1 altSubmit", Name)
    FilterMgr.AddCtrl("Text_Search", "Text", "x260 y95 w120 h25 ", QZLang.ButtonSearch)
    FilterMgr.AddCtrl("Edit_Search", "Edit", "x260 y120 w120 h25 ")
    FilterMgr.AddCtrl("Btn_Add", "Button", "x270 y160 w100 h25", QZLang.ButtonAddTo)  
     FilterMgr.AddCtrl("Btn_OK", "Button", "x270 y200 w100 h25", QZLang.ButtonOK)   
    FilterMgr.AddCtrl("Btn_New", "Button", "x270 y240 w100 h25", QZLang.ButtonNew)    
    FilterMgr.AddCtrl("Btn_Copy", "Button", "x270 y280 w100 h25", QZLang.ButtonCopy)    
    FilterMgr.AddCtrl("Btn_Modify", "Button", "x270 y320 w100 h25", QZLang.ButtonModify)
    FilterMgr.AddCtrl("Btn_DeleteForce", "Button", "x270 y360 w100 h25", QZLang.ButtonDeleteForce)    
    
    ;~ FilterMgr.SetFont(QZGlobal.FontSize, "s11 C800080 Bold Italic Microsoft YaHei")
    FilterMgr.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
    FilterMgr.AddCtrl("Text_FilteListr", "Groupbox", "x10 y405 w380 h150 ", QZLang.TextFilterList)
    ;~ FilterMgr.SetFont(QZGlobal.FontSize, "")
    FilterMgr.AddCtrl("Btn_Remove", "Button", "x270 y432 w100 h25", QZLang.ButtonRemove)
    FilterMgr.AddCtrl("TV_FilterList", "TreeView", "x20 y432 w230 h115 altsubmit -Lines +0x1000")
    FilterMgr.AddCtrl("Radio_Logic", "Text", "x260 y475 w120 h25 ", QZLang.FilterLogic)
    FilterMgr.AddCtrl("Radio_LogicAnd", "Radio", "x270 y500 w110 h25", QZLang.FilterLogicAnd)
    FilterMgr.AddCtrl("Radio_LogicOr" , "Radio", "x270 y525 w110 h25", QZLang.FilterLogicOr)
    

    SetExplorerTheme(FilterMgr.TV_ExistFilter.Hwnd)
    SetExplorerTheme(FilterMgr.TV_FilterList.Hwnd)
    FilterMgr.TV_ExistFilter.OnEvent("GUI_FilterMgr_EventEF")
    FilterMgr.TV_FilterList.OnEvent("GUI_FilterMgr_EventFL")
    FilterMgr.BTN_Add.OnEvent("GUI_FilterMgr_BTNAdd")
    FilterMgr.BTN_Remove.OnEvent("GUI_FilterMgr_BTNRemove")
    FilterMgr.BTN_New.OnEvent("GUI_FilterMgr_BTNNew")
    FilterMgr.BTN_Modify.OnEvent("GUI_FilterMgr_BTNModify")
    FilterMgr.BTN_More.OnEvent("GUI_FIlterMgr_More")
    FilterMgr.BTN_Copy.OnEvent("GUI_FIlterMgr_BTNCopy")
    FilterMgr.BTN_DeleteForce.OnEvent("GUI_FilterMgr_BTNDelete")
    FilterMgr.DDL_Type.OnEvent("GUI_FilterMgr_Search")
    FilterMgr.BTN_OK.OnEvent(Callback)
    If Strlen(CloseEvent)
    {
        FilterMgr.OnClose(CloseEvent)
        FilterMgr.BTN_Close.OnEvent(CloseEvent)
    } else {
        FilterMgr.BTN_Close.OnEvent("GUI_FilterMgr_Destroy")
    }
    FilterMgr.Edit_Search.OnEvent("GUI_FilterMgr_Search")
    FilterMgr.DataEF := {} ; 配置中已有的筛选器列表
    FilterMgr.DataFL := {} ; 使用的筛选器列表
    FilterMgr.UUIDListFL := {} ; 保存已有列表的UUID列表
    FilterMgr.DataMenu := "" ; 保存当前编辑的菜单项对象
    FilterMgr.Types := Types ; 筛选器类型对象，用于搜索
    FilterMgr.Show("", QZLang.TitleFilterMgr)
    FilterMgr.TV_FilterList.Focus()
}

GUI_FilterMgr_LoadData(objMenu){
    Global FilterMgr, gQZConfig
    FilterMgr.DataMenu := objMenu
    GUI_FilterMgr_Add(FilterMgr.TV_FilterList, objMenu.Filter)
    FilterMgr.TV_ExistFilter.SetNotReDraw()         ;禁止重绘，提高效率
    For uuid , objFilter In gQZConfig.Filters
    {
        If FilterMgr.UUIDListFL[uuid]
            Continue
        GUI_FilterMgr_Add(FilterMgr.TV_ExistFilter, {1:uuid})
    }
    FilterMgr.TV_ExistFilter.SetReDraw()        ;打开重绘，提高效率
    If objMenu.Options.FilterLogic
        FilterMgr.Radio_LogicOr.SetText(True)
    Else
        FilterMgr.Radio_LogicAnd.SetText(True)
    
    FilterMgr.TV_FilterList.SetDefault()
}
GUI_FilterMgr_Dump(){
    Global FilterMgr
    Return FilterMgr
}
GUI_FilterMgr_EventEF(){
    Global FilterMgr, FilterEditor
    FilterMgr.TV_ExistFilter.SetDefault()
    If A_EventInfo
    {
        If WinExist("ahk_id " FilterEditor.Hwnd)
            Return
        FilterMgr.FocusEF := True
        TV_Modify(A_EventInfo, "Select")
        If (A_GuiEvent = "DoubleClick") 
        {
            UUID := FilterMgr.DataEF[A_EventInfo]
            FilterMgr.DataEF.Delete(A_EventInfo)
            TV_Delete(A_EventInfo)
            GUI_FilterMgr_Add(FilterMgr.TV_FilterList, {1:uuid})
        }
        If (A_GuiEvent = "RightClick")
            GUI_FilterMgr_BTNModify()
    }
}
GUI_FilterMgr_EventFL(){
    Global FilterMgr, FilterEditor
    FilterMgr.TV_FilterList.SetDefault()
    If A_EventInfo
    {
        If WinExist("ahk_id " FilterEditor.Hwnd)
            Return
        FilterMgr.FocusEF := False
        TV_Modify(A_EventInfo, "Select")
        If (A_GuiEvent = "DoubleClick") && (FilterMgr.BTN_More.GetText() = QZLang.ButtonLess)
        {
            UUID := FilterMgr.DataFL[A_EventInfo]
            FilterMgr.DataFL.Delete(A_EventInfo)
            TV_Delete(A_EventInfo)
            GUI_FilterMgr_Add(FilterMgr.TV_ExistFilter, {1:UUID})
        }
        If (A_GuiEvent = "RightClick")
            GUI_FilterMgr_BTNModify()
    }
}
GUI_FilterMgr_Add(objTV, objList){
    Global FilterMgr, gQZConfig
    objTV.SetDefault()
    Loop % ObjList.MaxIndex()
    {
        UUID := objList[A_Index]
        Obj := gQZConfig.Filters[UUID]
        newID := TV_Add(Obj.Name, 0, "Select")
        TV_Modify(0, "Sort")
        FilterMgr.UUIDListFL[UUID] := False
        If (objTV.hwnd = FilterMgr.TV_ExistFilter.hwnd)
        {
            FilterMgr.DataEF[newID] := UUID
        }
        Else
        {
            FilterMgr.DataFL[newID] := UUID
            FilterMgr.UUIDListFL[UUID] := True
        }
    }
}


GUI_FilterMgr_Search(){
    Global FilterMgr, gQZConfig
    FilterMgr.SearchSignal := True
    SetTimer, _FilterMgr_Search, -200
    Return
    _FilterMgr_Search:
        objGUI := GUI_FilterMgr_Dump()
        objGUI.SearchSignal := False
        keyword := objGui.Edit_Search.GetText()
        objGUI.TV_ExistFilter.SetDefault()
        TV_Delete()
        objGUI.DataEF := {}
        numPos  := objGUI.DDL_Type.GetText()
        strType := objGUI.Types[numPos]
        For UUID, objFilter In gQZConfig.Filters
        {
            If objGUI.SearchSignal
            {
                ;TV_Delete()
                Return
            }
            If objGUI.UUIDListFL[uuid]
                Continue
            obj := gQZConfig.Filters[UUID]
            If (numPos > 1)
            {
                If IsObject(obj[strType]) && !obj[strType].Method 
                    Continue
                Else If !IsObject(obj[strType]) && !Strlen(obj[strType]) 
                    Continue
            }
            If TCMatch(obj.name, keyword)
                GUI_FilterMgr_Add(objGUI.TV_ExistFilter, {1:uuid})
        }
    Return
}
GUI_FilterMgr_BTNAdd(){
    Global FilterMgr
    FilterMgr.TV_ExistFilter.SetDefault()
    SelectID := TV_GetSelection()
    If !SelectID
        SelectID := TV_GetChild(0)
    If SelectID
    {
        UUID := FilterMgr.DataEF[SelectID]
        FilterMgr.DataEF.Delete(SelectID)
        TV_Delete(SelectID)
        GUI_FilterMgr_Add(FilterMgr.TV_FilterList, {1:UUID})
    }
}
GUI_FilterMgr_BTNNew(){
    Gui_FilterEditor_Load("GUI_FilterMgr_BTNNewDo")
}
GUI_FilterMgr_BTNNewDo(){
    Global FilterMgr, gQZConfig

    objFilter := QZ_CreateConfig_Filter()
    GUI_FilterEditor_Save(objFilter)
    GUI_FilterEditor_Destroy()

    strUUID := UUIDCreate(1, "U")
    gQZConfig.Filters[strUUID] := objFilter ; 保存到filters
    objMenu := FilterMgr.DataMenu
    If !(Index:=objMenu.Filter.MaxIndex())
        Index := 0
    objMenu.Filter[Index+1] := strUUID ; 保存到objMenu中的filter
    GUI_FilterMgr_Add(FilterMgr.TV_FilterList, {1:strUUID})
}
GUI_FilterMgr_BTNCopy(){
    Global FilterMgr, gQZConfig
    filterMgr.TV_ExistFilter.SetDefault()
    SelectID := TV_GetSelection()
    If SelectID
    {
        oldFilter := gQZConfig.Filters[FilterMgr.DataEF[SelectID]]
        newFilter := ObjectCopy(oldFilter)
        newFilter.Name := newFilter.Name . QZLang.SuffixCopy
        strUUID := UUIDCreate(1, "U")
        gQZConfig.Filters[strUUID] := newFilter
        GUI_FilterMgr_Add(FilterMgr.TV_ExistFilter, {1:strUUID})
    }
}
GUI_FilterMgr_BTNModify(){
    Global FilterMgr
    If FilterMgr.FocusEF
    {
        ObjTV := FilterMgr.TV_ExistFilter
        objData := FilterMgr.DataEF
    }
    Else
    {
        ObjTV := FilterMgr.TV_FilterList
        objData := FilterMgr.DataFL
    }
    ObjTV.SetDefault()
    SelectID := TV_GetSelection()
    If SelectID
    {
        GUI_FilterEditor_Load("GUI_FilterMgr_BTNModifyOK")
        GUI_FilterEditor_LoadData(objData[SelectID])
    }
}
GUI_FilterMgr_BTNModifyOK(){
    Global FilterMgr, gQZConfig
    If FilterMgr.FocusEF
    {
        ObjTV := FilterMgr.TV_ExistFilter
        objData := FilterMgr.DataEF
    }
    ElsE
    {
        ObjTV := FilterMgr.TV_FilterList
        objData := FilterMgr.DataFL
    }
    objTV.SetDefault()
    SelectID := TV_GetSelection()
    If SelectID
    {
        UUID := objData[SelectID]
        obj := gQZConfig.Filters[UUID].Clone()
        GUI_FilterEditor_Save(obj)
        gQZConfig.Filters[UUID] := obj
    }
    objTV.SetDefault()
    TV_Modify(SelectID, "", obj.Name)
    GUI_FilterEditor_Destroy()
}
GUI_FilterMgr_BTNDelete(){
    Global FilterMgr, gQZConfig
    filterMgr.TV_ExistFilter.SetDefault()
    SelectID := TV_GetSelection()
    If SelectID
    {
        gQZConfig.Filters.Delete(FilterMgr.DataEF[SelectID])
        FilterMgr.DataEF.Delete(SelectID)
        TV_Delete(SelectID)
    }
}

GUI_FilterMgr_BTNRemove(){
    Global FilterMgr
    FilterMgr.TV_FilterList.SetDefault()
    SelectID := TV_GetSelection()
    If !SelectID
        SelectID := TV_GetChild(0)
    If SelectID
    {
        UUID := FilterMgr.DataFL[SelectID]
        FilterMgr.DataFL.Delete(SelectID)
        TV_Delete(SelectID)
        GUI_FilterMgr_Add(FilterMgr.TV_ExistFilter, {1:UUID})
    }
}
GUI_FilterMgr_Save(ByRef aMenu){
    Global FilterMgr
    objFilter := [], Index := 1
    For id , UUID In FilterMgr.DataFL
        ObjFilter[Index++] := UUID
    aMenu.Filter := objFilter
    aMenu.Options.FilterLogic := FilterMgr.Radio_LogicOr.GetText()
}
GUI_FilterMgr_Destroy(){
    Global FilterMgr
    FilterMgr.Destroy()
}

