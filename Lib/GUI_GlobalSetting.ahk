﻿;~ #Include d:\KawvinApps\QuickZ3\Lib\Class_QZLang.ahk
;~ #Include d:\KawvinApps\QuickZ3\Lib\Class_GUI2.ahk
;~ #Include d:\KawvinApps\QuickZ3\Lib\Class_QZGlobal.ahk
;~ #Include d:\KawvinApps\QuickZ3\Lib\QZ_ReadConfig.ahk
;~ #Include d:\KawvinApps\QuickZ3\lib\class_json.ahk
;~ #Include d:\KawvinApps\QuickZ3\lib\QZ_API.ahk
;~ #Include d:\KawvinApps\QuickZ3\lib\_Struct.ahk
;~ Global  gQZConfig, QZGlobal
;~ Global gQZConfig, gQZShell, QZGlobal
;~ gQZConfig := QZ_ReadConfig(QZGlobal.Config)  ;调用Ahk文件：QZ_ReadConfig.ahk，作用：读取配置文件
;~ QZ_WriteConfig(QZ_GetConfig(), QZGlobal.Config)
#Include %A_ScriptDir%\lib\GUI_ItemFilterMode.ahk
GUI_GlobalSetting_Load(Callback, CloseEvent)
{
    Global GlobalSetting,gQZConfig
	;~ cons_fontsize := s10
    GlobalSetting := new GUI2("GlobalSetting", "+Lastfound +Theme -DPIScale")
    ;~ GlobalSetting.SetFont(%cons_fontsize%, "Microsoft YaHei")
	cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    GlobalSetting.SetFont(cons_fontsize, "Microsoft YaHei")
	GlobalSetting.AddCtrl("MyTab", "Tab2",  " x5 y5 h390 w470", QZLang.TabSetting)
	GlobalSetting.AddCtrl("LV_Hotkey", "ListView", "x15 y40 w450 h180 grid altsubmit",QZLang.ListViewHotkey)
	GlobalSetting.AddCtrl("GB_1", "GroupBox", "x15 y220 w450 h125", "")
    GlobalSetting.AddCtrl("Text_HotKey", "Text",  "x25 y242 w100", QZLang.TextHotKey)
    GlobalSetting.AddCtrl("Edit_HotKey", "Edit", "x60 y240 w120", "")
	GlobalSetting.AddCtrl("Text_HotkeyMode", "Text",  "x196 y242 w110", QZLang.HotkeyMode)
    GlobalSetting.AddCtrl("Edit_HotkeyMode", "Edit", "x260 y240 w195 h26 readonly", "")
	GlobalSetting.AddCtrl("Checkbox_HotkeySep", "Checkbox", "x60 y275 h26", QZLang.HotkeySep)
    GlobalSetting.AddCtrl("Button_DefindMode", "Button", "x260 y275 w90 h28", QZLang.ButtonDefindMode)
	GlobalSetting.AddCtrl("Button_ManagerMode", "Button", "x365 y275 w90 h28", QZLang.ButtonManagerMode)
	GlobalSetting.AddCtrl("Text_TextAction", "Text",  "x25 y312 w100", QZLang.TextAction)
	;GlobalSetting.AddCtrl("DDL_HotkeyAction", "DDL", "x60 y310 w395 r4 altsubmit", QZLang.HotkeyAction )
	GlobalSetting.AddCtrl("DDL_HotkeyAction", "DDL", "x60 y310 w395", QZLang.HotkeyAction )
    GlobalSetting.AddCtrl("Button_SaveHotKey", "Button", "x296 y355 w80 h28", QZLang.ButtonApply)
	GlobalSetting.AddCtrl("Button_Delete", "Button", "x386 y355 w80 h28", QZLang.ButtonDelete)
	GlobalSetting.LV_Hotkey.SetDefault()
    LV_ModifyCol(1, 60)
    LV_ModifyCol(2, 100)
    LV_ModifyCol(3, 140)
	LV_ModifyCol(4, 135)
	GlobalSetting.MyTab.switch(2)
	GlobalSetting.AddCtrl("LV_GlobalSetting", "ListView", "x15 y40 w450 h270 grid altsubmit",QZLang.GlobalSetting)
	GlobalSetting.AddCtrl("Text_GlobalOption", "Text",  "x15  y323 w90", QZLang.GlobalOption)
    GlobalSetting.AddCtrl("Edit_GlobalOption", "Edit", "x85  y320 w290 r1 ReadOnly", "")
	GlobalSetting.AddCtrl("Text_GlobalValue", "Text",  "x15 y358 w90", QZLang.GlobalValue)
    GlobalSetting.AddCtrl("Edit_GlobalValue", "Edit", "x85 y355 w290 r1", "")
    GlobalSetting.AddCtrl("Button_Browse", "Button", "x386 y355 w80 h28", QZLang.ButtonBrowse)
	;~ GlobalSetting.AddCtrl("Button_ApplyGlobal", "Button", "x386 y320 w80 h28", QZLang.ButtonApply)
	GlobalSetting.LV_GlobalSetting.SetDefault()
    LV_ModifyCol(1, 100)
    LV_ModifyCol(2, 100)
    LV_ModifyCol(3, 500)	
	GlobalSetting.Button_DefindMode.OnEvent("GUI_GlobalSetting_DefindMode")
	GlobalSetting.Button_ManagerMode.OnEvent("GUI_GlobalSetting_ManagerMode")
	GlobalSetting.Button_SaveHotKey.OnEvent("GUI_GlobalSetting_SaveHotKey")
	GlobalSetting.Button_Delete.OnEvent("GUI_GlobalSetting_Delete")
	GlobalSetting.Button_Browse.OnEvent("GUI_GlobalSetting_Browse")
	;~ GlobalSetting.Button_ApplyGlobal.OnEvent("GUI_GlobalSetting_Apply")
	GlobalSetting.LV_GlobalSetting.OnEvent("GUI_GlobalSetting_GlobalSettingListViewEvent")
    GlobalSetting.LV_Hotkey.OnEvent("GUI_GlobalSetting_HotkeyListViewEvent")
    GlobalSetting.Edit_GlobalValue.OnEvent("GUI_GlobalSetting_GlobalValue")
    GlobalSetting.Callback := Callback
    GlobalSetting.Show("h400 w480 ",QZLang.TitleSetting)
	GUI_GlobalSetting_LoadData()
	Return GlobalSetting
}

GUI_GlobalSetting_LoadData()
{
	Global GlobalSetting, gQZConfig
	;读取热键
    GlobalSetting.LV_Hotkey.SetDefault()
    LV_Delete()
	If Not Isobject(gQZConfig.hotkeys)
        gQZConfig.hotkeys:= {}
    for HotKeyName , HotKeyObject in gQZConfig.Hotkeys
    {
        LV_Add("", HotKeyName, HotKeyObject.Action,HotKeyObject.FilterMode, HotKeyObject.Comment )
    }
	;读取全局变量
	GlobalSetting.LV_GlobalSetting.SetDefault()
    LV_Delete()
	If Not Isobject(gQZConfig.setting.global)
        gQZConfig.setting.global := {}
	;~ cons_Setting =
	;~ (LTrim, Join`r`n
	  ;~ TimeOut|0.4|超时时间(默认0.4秒)
	  ;~ Method|0|复制使用的方法(默认值:0,使用ctrl+c,可设置1使用ctrl+Insert)
	  ;~ CandyCmd|0|使用Candy的配置语法(默认值:0,不使用，设置1使用)
	  ;~ Editor|Notepad.exe|AHK代码模式外部编辑器路径
	  ;~ EditorParam||AHK代码模式外部编辑器参数
	  ;~ DPIScale|10|如果使用了屏幕放大125`%或者以上，调小这个值为9或者8
	  ;~ NotSetting|0|设置1来取消自动添加"配置(&S)"菜单项到菜单最下方
	  ;~ UserEnv_File||用户变量配置文件的路径
	;~ )
	cons_Setting =
	(LTrim, Join`r`n
	  TimeOut|0.4|超时时间(默认0.4秒)
	  Editor|Notepad.exe|AHK代码模式外部编辑器路径
	  EditorParam||AHK代码模式外部编辑器参数
	  DPIScale|9|如果使用了屏幕放大125`%或者以上，调小这个值为8或者7
	)
    Loop, Parse, cons_Setting, `n ,`r
    {
        Stringsplit, option ,A_loopfield, |
        if strlen(gQZConfig.setting.global[Option1])
            LV_Add("", option1, gQZConfig.setting.global[Option1], option3)
        else
            LV_Add("", option1, option2, option3)
    }
}
GUI_GlobalSetting_DefindMode()
{
	Global GlobalSetting, gQZConfig
    MouseGetPos,CurX,CurY,,,,  a
    Menu, FilterModesDefind, Add
    Menu, FilterModesDefind, DeleteAll
	objMode := gQZConfig.Setting.MenuZ.FilterModes
	Loop % objMode.MaxIndex()
	{
		strName := objMode[A_Index]
		Menu, FilterModesDefind, Add, %strName%, _MenuHandle_HotKey_FilterModesDefind
	}
	Menu, FilterModesDefind, Add
	Menu, FilterModesDefind, Add, 无模式, _MenuHandle_HotKey_FilterModesDefind
    Menu, FilterModesDefind, Show, %PosX%, %PosY%
    Return
    _MenuHandle_HotKey_FilterModesDefind:
        GlobalSetting.Default()
		if(A_ThisMenuItem="无模式")
			GlobalSetting.Edit_HotkeyMode.settext("")
		else
			GlobalSetting.Edit_HotkeyMode.settext(A_ThisMenuItem)
    Return
}
GUI_GlobalSetting_ManagerMode()
{
    GUI_ItemFilterMode_Load("","GUI_GlobalSetting_ReloadMode")
}
GUI_GlobalSetting_ReloadMode()
{
	GUI_ItemFilterMode_Destroy()
	GUI_GlobalSetting_LoadData()
}
GUI_GlobalSetting_SaveHotKey()
{
	Global GlobalSetting, gQZConfig
	GlobalSetting.Default()
	HotkeyName:=GlobalSetting.Edit_HotKey.GetText()
	HotkeyFilterMode:=GlobalSetting.Edit_HotkeyMode.GetText()
	HotkeySep:=GlobalSetting.Checkbox_HotkeySep.GetText()
	HotkeyObject:=GlobalSetting.DDL_HotkeyAction.GetText()
    HotkeyAction  := RegExReplace(HotkeyObject, "\s\s.*$")
    if Instr(HotkeyObject, "  ")
        HotkeyComment := RegExReplace(HotkeyObject, "^.*\s\s")
    else
        HotkeyComment := ""
    if not strlen(HotkeyName)
        return
	if not strlen(HotkeyAction)
        return
	GlobalSetting.LV_Hotkey.SetDefault()
    if IsObject(gQZConfig.hotkeys[HotkeyName])
    {
        Loop, % LV_GetCount()
        {
            LV_GetText(String, A_Index, 1)
            if (string = HotkeyName )
                LV_Modify(A_Index, "focus select vis", HotkeyName, HotkeyAction, HotkeyFilterMode,HotkeyComment)
            else
                LV_Modify(A_Index, "-select" )
        }
    }
    else
    {
        LV_Add("focus vis", HotkeyName, HotkeyAction,HotkeyFilterMode,HotkeyComment)
    }
    gQZConfig.hotkeys[HotkeyName] := {Action: HotkeyAction, Comment: HotkeyComment, FilterMode: HotkeyFilterMode, Sep: HotkeySep}
	QZ_WriteConfig(gQZConfig,QZGlobal.Config)
    Send_WM_CopyData("reload")
}
GUI_GlobalSetting_Delete()
{
	Global GlobalSetting, gQZConfig
	GlobalSetting.Default()
	HotkeyName:=GlobalSetting.Edit_HotKey.GetText()
	HotkeyFilterMode:=GlobalSetting.Edit_HotkeyMode.GetText()
	HotkeySep:=GlobalSetting.Checkbox_HotkeySep.GetText()
	HotkeyObject:=GlobalSetting.DDL_HotkeyAction.GetText()
    HotkeyAction  := RegExReplace(HotkeyObject, "\s\s.*$")
    if Instr(HotkeyObject, "  ")
        HotkeyComment := RegExReplace(HotkeyObject, "^.*\s\s")
    else
        HotkeyComment := ""
	if not strlen(HotkeyName)
        return
	MsgBox, 36, 提示, 确认删除热键？`n`n【热键】：%HotkeyName%`n【功能】：%HotkeyAction%`n【说明】：%HotkeyComment%
	ifmsgbox,No
		return
	GlobalSetting.LV_Hotkey.SetDefault()
    Index := LV_GetNext(0, "Focus")
    LV_GetText(HotkeyName, Index, 1)
    LV_Delete(Index)
    gQZConfig.hotkeys.Remove(HotkeyName)
	GlobalSetting.Edit_HotKey.setText("")
	GlobalSetting.Edit_HotkeyMode.setText("")
	GlobalSetting.Checkbox_HotkeySep.setText(0)
	GlobalSetting.DDL_HotkeyAction.choose(0)
	QZ_WriteConfig(gQZConfig,QZGlobal.Config)
    Send_WM_CopyData("reload")
}
GUI_GlobalSetting_Browse()
{
	Global GlobalSetting, gQZConfig
	FileSelectFile, AppPath, 3, , 选择【外部编辑器】路径, 程序文件(*.exe)
	if ErrorLevel
		return
	if (AppPath !="")
		GlobalSetting.Edit_GlobalValue.setText(AppPath)
	else
		return
}

GUI_GlobalSetting_HotkeyListViewEvent()
{
	Global GlobalSetting, gQZConfig
    If not A_EventInfo 
        return
    If A_GuiEvent = Normal
    {
		GlobalSetting.LV_Hotkey.SetDefault()
        LV_GetText(HotkeyName, A_EventInfo, 1)
        LV_GetText(HotkeyAction, A_EventInfo, 2)
        LV_GetText(HotkeyComment, A_EventInfo, 3)
		GlobalSetting.Edit_HotKey.setText(HotkeyName)
		GlobalSetting.Edit_HotkeyMode.setText(gQZConfig.hotkeys[HotkeyName].FilterMode)
		if gQZConfig.hotkeys[HotkeyName].Sep
            GlobalSetting.Checkbox_HotkeySep.setText(1)
        else
            GlobalSetting.Checkbox_HotkeySep.setText(0)
        if strlen(HotkeyAction)
            GlobalSetting.DDL_HotkeyAction.choose(HotkeyAction)
        else
            GlobalSetting.DDL_HotkeyAction.choose(0)
	}
}
GUI_GlobalSetting_GlobalSettingListViewEvent()
{
	Global GlobalSetting, gQZConfig
	 If not A_EventInfo 
        return
    If A_GuiEvent = Normal
    {
		GlobalSetting.LV_GlobalSetting.SetDefault()
		LV_GetText(GlobalOption, A_EventInfo, 1)
        LV_GetText(GlobalValue, A_EventInfo, 2)
		GlobalSetting.Edit_GlobalOption.setText(GlobalOption)
		GlobalSetting.Edit_GlobalValue.setText(GlobalValue)
		if(GlobalOption="Editor")
			GlobalSetting.Button_Browse.Enable()
		else
			GlobalSetting.Button_Browse.Disable()
	}
}
;~ GUI_GlobalSetting_Apply()
GUI_GlobalSetting_GlobalValue()
{
	Global GlobalSetting,gQZConfig
	GlobalSetting.Default()
	option1:=GlobalSetting.Edit_GlobalOption.GetText()
	option2:=GlobalSetting.Edit_GlobalValue.GetText()
    gQZConfig.setting.global[option1] := option2
    LV_Modify(LV_GetNext(0, "Focused"), "col2", option2 )
}
GUI_GlobalSetting_Dump()
{
    Global GlobalSetting
    Return GlobalSetting
}

GUI_GlobalSetting_Destroy()
{
    Global GlobalSetting
    GlobalSetting.Destroy()
}
