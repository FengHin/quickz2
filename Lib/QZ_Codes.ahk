﻿Return
00356317-C8AE-454B-9D60-0611B4F369FB:
;&P  复制快捷方式目标路径
MySel :=QZData("files")
FileGetShortcut,%MySel%,MyTempfile
clipboard := MyTempfile
Return
00B12AFD-297E-48A8-9C55-5BC7D1C14186:
;Excel_字号变大
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    currentFontSize := Selection.Font.Size
    Selection.Font.Size := currentFontSize + 1
}
Return
0217BB35-93C4-4EFA-98E3-3597FDF947DB:
;Excel_删除选中行
    sendinput,^-
    sleep,30
    sendinput,!r
    sleep,30
    sendinput,{enter}
Return
0270DEAC-F512-4F52-9E2C-8F713A29B356:
;QQ浏览器_切换到【VIM模式】
send,{esc}
vimd.changemode("VIM模式")
MsgBox, 0, 提示, 【VIM模式】, 0.3
Return
02B257FD-5CDA-4CFA-AC0F-540F57E7C21B:
;Excel_工作表循环-切换工作表
global LastSelectSheetIndex
excel:=Excel_Get()
ActiveSheet:=excel.ActiveSheet
OldIndex:=LastSelectSheetIndex
try
	LastSelectSheetIndex:=excel.ActiveSheet.index
catch e
    LastSelectSheetIndex:=1
try
	excel.Worksheets(OldIndex).Select
catch e
	excel.Worksheets(1).Select
   

Return
0308B734-8104-4502-9D6E-72F71F1A5EBC:
;Excel_小数点1位
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0.0"
Return
04C695A3-56B6-4B24-A3EE-043C7B517C73:
;Excel_运行宏-临时3
;comObjactive("excel.application").run("Personal.xlsb!公式类_2粘贴数据公式")
excel:=Excel_Get()
;sendinput,^{enter}
;sleep,300
excel.Cells(1,6).select
Return
05D0E6F0-038D-4619-8A11-BECD8AF532C7:
;Phantom_文件-从多文档创建PDF
;^f12::
gui Destroy
gui New
Gui Font, s12
Gui Add, Text, x10 y15 w85 h26 +0x200, 文件目录：
Gui Add, Edit, x100 y15 w384 h26 vMyVar_FileDir
Gui Add, Button, x490 y15 w70 h26, PDF浏览
Gui Add, ComboBox, x100 y50 w160 vMyVar_FileExt,pdf|jpg|png|bmp 
Gui Add, Text, x10 y47 w85 h30 +0x200, 文件格式：
Gui Add, Edit, x10 y85 w550 h500 vMyVar_FileList
Gui Add, Button, x110 y600 w75 h39, PDF合并
Gui Add, Button, x250 y600 w75 h39, PDF清空
Gui Add, Button, x390 y600 w75 h39, PDF退出
Gui Font
Gui Show, w570 h650, 批量合并文件
Return

ButtonPDF浏览:
	;~ Gui PDFMarge:Default
	FileSelectFolder, MyVar_FileDir
	if ErrorLevel
		return
	if (MyVar_FileDir="")
		return
	MyVar_FileDir := RegExReplace(MyVar_FileDir, "\\$")  ; 移除默认的反斜线,如果存在.
	guicontrol,,MyVar_FileDir,%MyVar_FileDir%
return

ButtonPDF合并:
	;~ Gui PDFMarge:Default
	gui,Submit
	NotExistList:=
	ExistList:=
	if (MyVar_FileExt="")
		msgbox ,请选择文件后缀名！
	loop,Parse,MyVar_FileList,`n,`r
	{
		TemFileNameNoExt:=trim(A_LoopField)
		if (TemFileNameNoExt="")
			continue
		TemFileFullPath=%MyVar_FileDir%\%TemFileNameNoExt%.%MyVar_FileExt%
		ifnotexist,%TemFileFullPath%
			NotExistList.=TemFileNameNoExt "." MyVar_FileExt "`n" 
		else
			ExistList.=TemFileFullPath "`n" 
	}
	NotExistList:=RegExReplace(NotExistList,"\s*$","")   ;顺便删除最后的空白行，可根据需求注释掉
	ExistList:=RegExReplace(ExistList,"\s*$","")   ;顺便删除最后的空白行，可根据需求注释掉
	if strlen(NotExistList)
	{
		Clipboard=%NotExistList%
		MsgBox, 4, 提示, 以下文件不存在，是否继续？`n文件列表已复制到剪贴板，如有需要，请先粘贴保存`n`n%NotExistList%
	}
	IfMsgBox ,no
		goto ,ButtonPDF退出
	;AppPath:="C:\Program Files (x86)\foxit software\foxit phantom\Foxit Phantom.exe"
	AppPath:=QZData( "%Foxit_Phantom%" )
	run,"%AppPath%"
	sleep,2000
	#WinActivateForce
	WinActivate,,ahk_class classFoxitPhantom
	;~ sleep,1500
	sendinput,!f
	sleep,100
	sendinput,f
	sleep,100
	sendinput,m
	;~ ControlClick,Button2,多文档转换
	sendinput,{tab}
	loop,Parse,ExistList,`n,`r
	{
		TemFileFullPath:=trim(A_LoopField)
		;~ msgbox % TemFileFullPath
		sendinput,{enter}
		sleep,300
		Clipboard=%TemFileFullPath%
		;~ sendinput,{Raw}%TemFileFullPath%
		sendinput,^v
		sleep,300
		sendinput,{enter}
	}
	sendinput,!m
	goto ,ButtonPDF退出
return

ButtonPDF清空:
	;~ Gui PDFMarge:Default
	guicontrol,,MyVar_FileDir,
	guicontrol,,MyVar_FileList,
	guicontrol,,MyVar_FileExt,||
	guicontrol,,MyVar_FileExt,pdf|jpg|png|bmp 
	MyVar_FileDir:=
	MyVar_FileList:=
	MyVar_FileExt:=
return

ButtonPDF退出:
	;~ Gui PDFMarge:Destroy
    gui,Destroy
Return
05E84F4F-D6C2-415E-AB0B-CE887910F045:
;Excel_列宽减小
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    currentWidth := excel.Cells(Selection.Column, Selection.Column).ColumnWidth
    Selection.ColumnWidth := currentWidth -1
}
Return
0631FCA2-1C78-4695-969A-750F62455A85:
;FastStone_向左旋转并保存
WinGetClass, CurTitle,A
if RegExMatch(CurTitle,"TFullScreenWindow")		;ahk_class TFullScreenWindow
{
	sendinput,^!l
}
if RegExmatch(CurTitle,"FastStoneImageViewerMainForm")		;ahk_class FastStoneImageViewerMainForm.UnicodeClass
{
	WinMenuSelectItem, A, , 工具, JPEG 无损旋转图像,向左旋转
	;sendinput,^!l
}

Return
0785B1E9-2764-4C23-A494-9AEC29285702:
;&Z  打开快捷方式所指目录(资源管理器)
MySel:=QZData("files" )
FileGetShortcut,%MySel%,MyTemFile
SplitPath,MyTemFile,,MyOutDir
;MyCmdLine=explorer.exe /select,%dir%
;msgbox % MyCmdLine
;run,% MyCmdLine
;Run,explorer.exe /select`,%MyOutDir%
Run,explorer.exe /open`,%MyOutDir%
;Run,% "explorer.exe /select,dir"
;Run, % "explorer.exe /open,dir"
Return
07E53524-1B49-420E-97E4-B76DF8888D65:
;Excel_指定填充/字体颜色【快捷键大写】
;[CBlack:=1;黑];[CWhite:=2;白];[CRed:=3;红];[CGreen:=4;绿];[CBlue:=5;蓝];[CYellow:=6;黄];[CMagenta:=7;粉红];[CCyan:=8;青];[CGray25:=15;灰色25];[CGray40:=48;灰色40];[CGray50:=16;灰色50];[CPurple:=17;紫];[CLightRed:=22;浅红];[CSkyBlue:=33;天蓝];[CCyanGreen:=34;浅青绿];[CLightGreen:=35;浅绿];[CLightYellow:=36;浅黄];[CLightBlue:=37;浅蓝];[CRoseRed:=38;玫瑰红];[CLigthPurple:=39;浅紫];[COrangeBrown:=40;茶色];[CMiddleBlue:=41;浅蓝];[CWaterGreen:=42;水绿色];[CGrassGreen:=43;青草绿];[CGold:=44;金];[CLightOrange:=45;浅橙];[COrange:=46;橙];[CBlueGray:=47;蓝灰];[CDarkCyan:=49;深青];[CMiddleGreen:=50;中绿];[CTan:=53;褐];[CDarkBlue:=55;黑蓝];[CAuto:=-4105;自动];[CNone:=-4142;无色]
;global ExcelStatic
;    excel:=Excel_Get()
;    Selection:=excel.Selection
;    Selection.Interior.ColorIndex:=ExcelStatic.CYellow
;MyColorArray:={"r":3,"R":22,"b":33,"B":28,"y":6,"Y":36,"g":4,"G":35,"o":45,"O":40,"g1":15,"g2":48,"g3":16,"g4":56,"w":2,"k":1,"a":-4105,"n":-4142}
;Excel_指定填充/字体颜色【快捷键大写】（区别在于：MyValue:=MyColorArray2[MyIndex]）
MyColorArray1:={"r":3,"b":33,"y":6,"g":4,"o":45,"g1":15,"g2":48,"g3":16,"g4":56,"w":2,"k":1,"a":-4105,"n":-4142}
MyColorArray2:={"R":22,"B":28,"Y":36,"G":35,"O":40}
global VimD
MyType:=substr(VimD.HotKeyStr,1,1)
MyIndex:=substr(VimD.HotKeyStr,2)
if ((MyIndex+0)>0)
	MyIndex=g%MyIndex%
MyValue:=MyColorArray2[MyIndex]
excel:=Excel_Get()
Selection:=excel.Selection
try{
if (MyType="r")		;填充颜色
	Selection.Interior.ColorIndex:=MyValue
if (MyType="t")		;字体颜色
	Selection.font.ColorIndex:=MyValue
}
Return
07F499CB-F190-4171-861B-F682E69F1BC6:
;Excel_对齐-右
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.HorizontalAlignment := ExcelStatic.xlHAlignRight
Return
091855CF-8A5C-4140-8FAA-B79CF50CE1A8:
;Excel_插入列-当前单元格左侧
    sendinput,{AppsKey}
    sleep,30
    sendinput,i
    sleep,30
    sendinput,!c
    sleep,30
    sendinput,{enter}
Return
097BC415-F134-4383-AC9B-35D321614635:
;Excel_插入列-当前单元格右侧
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row,ActiveCell.column+1).select
    sendinput,{AppsKey}
    sleep,30
    sendinput,i
    sleep,30
    sendinput,!c
    sleep,30
    sendinput,{enter}
Return
09ADCED8-5F15-46E7-8A6E-EC30E63BCD39:
;&P  Ping测试（列表标签）
List:=QZData("{box:list[www.163.com][114.114.114.114][www.baidu.com]}")
;msgbox % List

run cmd.exe /k ping %list% -t
Return
09CE9C4D-CCBC-4049-9635-887E949DA58B:
;&T 在TC中打开
ExplorerPath := QZData("text")
TLAppPath:=QZData("%TOTALCMD%")
ifnotexist,%TLAppPath%
	return
IfWinNotExist	ahk_class TTOTAL_CMD
	Run,%TLAppPath%,,Max
IfWinNotActive ahk_class TTOTAL_CMD
{
	Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
	WinWait,ahk_class TTOTAL_CMD
	WinActivate
}
PostMessage 1075, 3001, 0, , AHK_CLASS TTOTAL_CMD
ControlSetText, Edit1, cd %ExplorerPath%, ahk_class TTOTAL_CMD
Sleep,400
ControlSend, Edit1, {Enter}, ahk_class TTOTAL_CMD
return
Return
0A8586C6-CA70-4C90-8458-C1899935B2A0:
;Excel_删除当前工作表
    excel:=Excel_Get()
try
    excel.ActiveWindow.SelectedSheets.delete
Return
0B094606-D0F3-42F6-A971-7051A9E133BC:
;TC_选中目录在【右侧窗口】新建标签并激活
;Postmessage, 1075, 3004, 0,, ahk_class TTOTAL_CMD
sendinput,^{right}
Return
0D9D2C36-E86A-4453-961C-8025129D619C:
;&X  迅雷下载(Test)
AppPath:=QZData("%Thunder%")
Clipboard:=QZData("{select}")
ifnotexist,%AppPath%
	return
Run,%AppPath%  ;,,Max
WinWaitActive ,ahk_class XLUEFrameHostWnd,,10
if ErrorLevel		;如果5秒没激活，则退出
	return
;WinActivate
sendinput,^n
WinWaitActive , ahk_class XLUEModalHostWnd,,10
if ErrorLevel		;如果5秒没激活，则退出
	return
;WinActivate
sendinput,^v
clipboard:=
Return
0E1BFE70-D543-4E93-8A56-3FD7E4FF90CD:
;Excel_注释-添加
excel:=Excel_Get()
ActiveCell:=excel.ActiveCell
CurRow:=ActiveCell.row
CurCol:=ActiveCell.Column
InputBox, NewString, 输入, 请输入注释内容, , 300,150
if ErrorLevel
	return
if (NewString="")
	return
else
	Clipboard:=NewString
sendinput,{AppsKey}
sleep,50
sendinput,M
sleep,300
sendinput,^v
sleep.300
try
send,{esc}{esc}
try
ActiveCell.select
Return
0F828A3C-4FE6-446F-B956-ED8CF14E8704:
;&T 测试这段 AHK 代码 PipeRun
CustomFunc_PipeRun(gMenuZ.Data.text)
return
Return
1071C56C-E36E-43D9-BC6F-91D0EF3763F9:
;Excel_切换正常查看/分页预览
    global ExcelStatic
excel:=Excel_Get()
 try{    
   If (excel.ActiveWindow.View = ExcelStatic.xlPageBreakPreview)
        excel.ActiveWindow.View := ExcelStatic.xlNormalView
    Else
        excel.ActiveWindow.View := ExcelStatic.xlPageBreakPreview
}
Return
10FAFDED-3578-4D7A-8C0B-949FEAEB56B5:
;&9  重命名 修改后缀为ZIP
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if InStr(FileExist(MyTemFile), "D")
		continue
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	MyNewFile =%MyOutDir%\%MyOutNameNoExt%.zip
	FileMove,%MyTemFile%,%MyNewFile%
}
Return
12391B5A-1A3D-490F-B286-BEC93B8A4C17:
;Excel_内部水平方向-点线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Selection.Borders(ExcelStatic.xlInsideHorizontal).LineStyle := ExcelStatic.xlDot
    Selection.Borders(ExcelStatic.xlInsideHorizontal).Weight := ExcelStatic.xlHairline
}
Return
12C15E97-C9F6-45F0-8FAD-C3CF17C24B21:
;Excel_选择第一个工作表
    excel:=Excel_Get()
try
    excel.Worksheets(1).Select
Return
12CA015B-6389-43EC-AA3D-A08000150BE9:
;Excel_保存Excel文件
    sendinput,^s
Return
1654BC89-931D-4C31-B54F-CC8B3CB09005:
;&G  智能各自压缩(Shift:加密 Caps:删除)
MyKeyStatus:=
if GetKeyState("Ctrl")
	MyKeyStatus.="Ctrl"
if GetKeyState("Shift")
	MyKeyStatus.="Shift"
if GetKeyState("Alt")
	MyKeyStatus.="Alt"
if GetKeyState("Win")
	MyKeyStatus.="Win"
if GetKeyState("CapsLock", "T")
	MyKeyStatus.="Caps"

if (instr(MyKeyStatus,"Shift")>0)
	InputBox, MyPsw, 提示,`n请输入加密密码`n`n如果为空，则默认不加密`n加密文件名,密码形式为 xxxx`|x ，压缩格式为7z
else
	MyPsw:=

MySel= % QZData("files")
;msgbox % MySel

MyZipFile:=

if (instr(MyKeyStatus,"Caps")>0)
	MyDelete:=1
else
	MyDelete:=0

Zip7=%A_ScriptDir%\Apps\7-Zip\7zg.exe

loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,,MyOutDir,,MyOutNameNoExt
	MyFileList:=
	MyZipFile:=MyOutDir
	if InStr(FileExist(MyTemFile), "D")
	{
		Loop, %MyTemFile%\* ,1   ;1表示获取文件夹文件夹.
		{
			if A_Index=1
			{
				File=% A_LoopFileFullPath
				continue
			}
			File.= "`r" . "`n" . A_LoopFileFullPath
		}
		MyFileList := RegExReplace(File, "\r\n", """ """) 
	} else {
		MyFileList:=MyTemFile
	}
	;msgbox % MyFileList
		MyZipFile.= "\" MyOutNameNoExt
		MySub_7ZipAddEach(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete)
}

;~ 压缩过程，Zip7=7Zip路径,MyFileList=要压缩的文件列表,MyZipFile=压缩文件名称，MyPsw=密码,MyDelete=是否删除，1为删除
;~ MySub_7ZipAddEach(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete:=0)
MySub_7ZipAddEach(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete:=0)
{
	if MyPsw!=
	{
		StringSplit,MyArray_PSW,MyPSW,`|
		if MyArray_PSW2!=
		{
			MyZipFile:=MySub_CheckExistEach(MyZipFile,"7z")
			MyCmdLine=%Zip7% a "%MyZipFile%" `-p%MyArray_PSW1% -mhe "%MyFileList%"
		} else {
			MyZipFile:=MySub_CheckExistEach(MyZipFile,"zip")
			MyCmdLine=%Zip7% a "%MyZipFile%" `-p%MyArray_PSW1% "%MyFileList%"
		}
	} else {
		MyZipFile:=MySub_CheckExistEach(MyZipFile,"zip")
		MyCmdLine=%Zip7% a "%MyZipFile%" "%MyFileList%"
	}
	Run,%MyCmdLine%,,UseErrorLevel
	if MyDelete
	{
		loop,Parse,MyFileList,`n,`r
		{
			if InStr(FileExist(A_LoopField), "D")
				FileRemoveDir,%A_LoopField%
			else
				FileDelete,%A_LoopField%
		}
	}
}

MySub_CheckExistEach(FileNameNoExt,ext)
{
	IfExist,%FileNameNoExt%.%ext%   ;已经存在了以“首层文件夹命名”的文件夹，怎么办？
	{
		Loop
		{
			FolderName=%FileNameNoExt%( %A_Index% ).%ext%
			If !FileExist( FolderName )
			{
				return, %FolderName%
				break
			}
		}
		return
	}
	FolderName=%FileNameNoExt%.%ext%
	return, %FolderName%
}
Return
16A2BA1C-C12C-4B30-A610-036137F64F20:
;TC_删除
Postmessage, 1075, 908, 0,, ahk_class TTOTAL_CMD
Return
16BD470C-CBB5-41E2-9787-ABB8FF52CEC2:
;Excel_显示页面设置对话框
    global ExcelStatic
excel:=Excel_Get()
try{
    If (excel.Version> 11)
        excel.Dialogs(ExcelStatic.xlDialogPageSetup).Show
    Else
        ;For Excel 2003
        excel.Dialogs(ExcelStatic.xlDialogPageSetup).Show
}
Return
182BD32F-17A5-4273-89D5-B903E6AA4603:
;Excel_边框-粗实线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeTop).Weight := ExcelStatic.xlMedium
    Selection.Borders(ExcelStatic.xlEdgeBottom).Weight := ExcelStatic.xlMedium
    Selection.Borders(ExcelStatic.xlEdgeRight).Weight := ExcelStatic.xlMedium
    Selection.Borders(ExcelStatic.xlEdgeLeft).Weight := ExcelStatic.xlMedium
}
Return
18AAAE83-BB21-4A31-884A-3C8786B7A009:
;&R  普通正则式表达式转成Send形式
;给正则式中特殊的字符自动加上花括号，以便可用于Send等指令中，应用于SendRaw不方便的情况。
WinActivate % "ahk_class " QZData("winclass")
WinWaitActive % "ahk_class " QZData("winclass")
Send % TransRegEx(QZData("text"))
return

TransRegEx(RegExText) {
	Loop 2
	{
		RegExText := RegExReplace(RegExText, "{", "{{}")
		RegExText := RegExReplace(RegExText, "(?<=[^{{])\}", "{}}")
		Arr=^,!,+,#
		Loop, Parse, Arr, `,
			RegExText := RegExReplace(RegExText, "((?<=[^{])\" A_loopfield "(?=[^}]))|^\" A_loopfield "|\" A_loopfield "$", "{" A_loopfield "}")
	}
	return RegExText
}
Return
18FCCDBA-811C-41C8-B9B2-A09A7FDC4AC0:
;Excel_设置/取消下划线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{
    If (Selection.Font.Underline = ExcelStatic.xlUnderlineStyleNone)
        Selection.Font.Underline := ExcelStatic.xlUnderlineStyleSingle
    Else
        Selection.Font.Underline := ExcelStatic.xlUnderlineStyleNone
}
Return
1AC38904-338C-49B3-A8F6-AEAC20969A54:
;Phantom_页面管理-旋转-全部-逆90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{tab}
sendinput,{up}
sendinput,{up}
sendinput,{up}
sendinput,{enter}
Return
1AC7CC7B-2EAA-43F6-955B-CA76CCE7E90B:
;QQ浏览器_新建标签
sendinput,^t
Return
1DC15F54-5E00-41FA-9DD6-480456F1D2DB:
;通用_PageDown
Send {PgDn}
Return
1DE17ED0-BFB7-431C-9D8F-B98588EB6D25:
;&Z  打开当前目录(资源管理器)
CurWinClass:=QZData("winclass") ;将获取的class名赋值给用户变量
if (CurWinClass="TTOTAL_CMD") ;如果当前激活窗口为TC
{
	IfWinNotActive ahk_class TTOTAL_CMD
	{
		Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
		WinWait,ahk_class TTOTAL_CMD
		WinActivate
	}
	Postmessage, 1075, 332, 0,, ahk_class TTOTAL_CMD	;光标定位到焦点地址栏
	sleep 300
	PostMessage,1075,2029,0,,ahk_class TTOTAL_CMD ;获取路径
	sleep 100
	DirectionDir:=Clipboard
}
If(DirectionDir="ERROR")		;错误则退出
	return
;在资源管理器中打开
run, %DirectionDir%
return

Return
1DE325F4-6B1F-4213-B11F-79F93B2E0910:
;Excel_内部水平方向-实线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Selection.Borders(ExcelStatic.xlInsideHorizontal).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlInsideHorizontal).Weight := ExcelStatic.xlThin
}
Return
1E8B455A-D4C3-4A48-9CAD-5BFF83176DDC:
;Excel_填充当前单元格-以左侧单元格值
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row,ActiveCell.column):=excel.cells(ActiveCell.row,ActiveCell.column-1)
Return
1EE25B02-3629-4B9D-802A-03E5428FCF25:
;Excel_删除行-从当前行到顶部
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_rows_string:="1:" . topR
    Selected_range :=  excel.ActiveSheet.Range(Selected_rows_string)
    Selected_range.delete
}
Return
1F1B4080-2337-489F-82BF-EF0E979962E8:
;左键事件监控 (&L)
Run "%A_AhkPath%"  "\QuickZ\Apps\AutoHotkey\左键事件监控.ahk"
Return
1F23A818-A5C8-477A-BC5A-ABC3ED7E3E51:
;&T  窗口置顶/取消置顶
iHwnd=
iHwnd := gMenuZ.Data.hwnd
if iHwnd=
	WinGet, iHwnd, ID, A
wTitle=
WinGetTitle, wTitle, ahk_id %iHwnd%
X=
Y=
Width=
Height=
WinSet, AlwaysOnTop, Toggle, ahk_id %iHwnd%
WinGetPos, X, Y, Width, Height, ahk_id %iHwnd%
Y:=Y+5
WinGet, ExStyle, ExStyle, ahk_id %iHwnd%
if (ExStyle & 0x8)  ; 0x8 为 WS_EX_TOPMOST.
;  ... 窗口处于置顶状态, 执行适当的动作.
{
	WinSetTitle, ahk_id %iHwnd%, , %wTitle%_已置顶
	SplashImage Off
	SplashImage, % "",X%X% Y%Y% W100 B fs10 CTFFFFFF CW000000,% "窗口已置顶", , 切换模式提示
	WinSet, Transparent, 180, ahk_class AutoHotkey2
	sleep 1300
	SplashImage Off
}
else
{
	wTitle:=RegExReplace(wTitle, "_已置顶")
	WinSetTitle, ahk_id %iHwnd%, , %wTitle%
	SplashImage Off
	SplashImage, % "",X%X% Y%Y% W100 B fs10 CTFFFFFF CW000000,% "取消置顶", , 切换模式提示
	WinSet, Transparent, 180, ahk_class AutoHotkey2
	sleep 1300
	SplashImage Off
}
return
Return
1FB69A68-BD92-48E3-A60F-534EA1D75A1C:
;Excel_显示筛选内容列表
    FilteredRowColon:=
    FilteredRow:=
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
try{
    FilteredRowColon := strSplit(excel.ActiveSheet.AutoFilter.Range.Rows(1).Address, "$")[3]
    FilteredRow := strSplit(FilteredRowColon, ":")[1]
    excel.Cells(FilteredRow, ActiveCell.Column).Activate
    Sendinput,!{DOWN}
}
Return
20CBF2D4-ED8E-492D-9300-1798EE37F5B3:
;Excel_选中行
    excel:=Excel_Get()
    Selection:=excel.Selection
    ;~ ;Rngs_left := Selection.Column
    ;~ ;Rngs_top := Selection.Row
    ;~ ;Rngs_right := Rngs_left + Selection.Columns.Count - 1
    ;~ ;Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    ;~ ;leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    ;~ ;rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    ;~ ;topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    ;~ ;bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    ;~ ;Selected_rows_string:=topR . ":" . bottomR
    ;~ ;Selected_range :=  excel.ActiveSheet.Range(Selected_rows_string)
    ;~ ;Selected_range.select
try
    Selection.EntireRow.Select
Return
2131FED8-AE46-4185-8257-8A5931280F8E:
;TC_重命名
Postmessage, 1075, 1002, 0,, ahk_class TTOTAL_CMD
Return
21A3CBEE-B63D-4B86-813D-3C939B78186B:
;Phantom_切换到【VIM模式】
send,{esc}
vimd.changemode("VIM模式")
MsgBox, 0, 提示, 【VIM模式】, 0.3
Return
222B1EA1-B74E-48E0-B0DD-0307AEA06244:
;Excel_自动列宽
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_top_left := leftC . Rngs_top
    Selected_bottom_right := rightC .  Rngs_bottom
    Selected_range_string := Selected_top_left .  ":" . Selected_bottom_right
    Selected_range :=  excel.ActiveSheet.Range(Selected_range_string)
    Selected_range.columns.autofit
}
Return
23F20DA8-4C90-4246-A462-0D18174FE19C:
;Phantom_页面管理-旋转-全部-顺90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
sendinput,{tab}
sendinput,{up}
sendinput,{up}
sendinput,{up}
sendinput,{enter}
Return
258ED914-40B8-4B76-BA9F-7562C3F290B1:
;Excel_行高增大
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    currentHeight := excel.Cells(Selection.Row, Selection.Row).RowHeight
    Selection.RowHeight := currentHeight +1
}
Return
26038538-1749-4D72-8499-A10CAB1EE95F:
;Phantom_页面管理-删除从到
;send !o
;send d
WinMenuSelectItem, A, , 页面管理, 删除页面

Return
2782FB15-F241-4ABB-A1B9-E4FDC486A37E:
;通用_另存为（Ctrl+Shift+S）
sendinput,^+s
Return
28AC147A-E20B-4D33-A74C-3C6C854D8545:
;Excel_设置打印区域
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    excel.ActiveSheet.PageSetup.PrintArea := Selection.Address
Return
2925BE1A-E343-4550-B624-2F6843FA0F7E:
;Excel_自动行高
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_top_left := leftC . Rngs_top
    Selected_bottom_right := rightC .  Rngs_bottom
    Selected_range_string := Selected_top_left .  ":" . Selected_bottom_right
    Selected_range :=  excel.ActiveSheet.Range(Selected_range_string)
    Selected_range.rows.autofit
}
Return
29CF462A-6517-49B9-B02F-D356F7093B70:
;Excel_筛选-数值小于等于（<=）的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("<=", ActiveCell.Value,"fu")
Return
2B14C1D1-36DD-4489-94C1-CA60235E0AD8:
;Spy++ (&S)
Run "%A_AhkPath%" "\QuickZ\Apps\AutoHotkey\Ahkspy++.ahk"
Return
2B8452CA-A525-4DD4-9F0F-D726635078CA:
;Excel_指定填充/字体颜色【快捷键小写】
;[CBlack:=1;黑];[CWhite:=2;白];[CRed:=3;红];[CGreen:=4;绿];[CBlue:=5;蓝];[CYellow:=6;黄];[CMagenta:=7;粉红];[CCyan:=8;青];[CGray25:=15;灰色25];[CGray40:=48;灰色40];[CGray50:=16;灰色50];[CPurple:=17;紫];[CLightRed:=22;浅红];[CSkyBlue:=33;天蓝];[CCyanGreen:=34;浅青绿];[CLightGreen:=35;浅绿];[CLightYellow:=36;浅黄];[CLightBlue:=37;浅蓝];[CRoseRed:=38;玫瑰红];[CLigthPurple:=39;浅紫];[COrangeBrown:=40;茶色];[CMiddleBlue:=41;浅蓝];[CWaterGreen:=42;水绿色];[CGrassGreen:=43;青草绿];[CGold:=44;金];[CLightOrange:=45;浅橙];[COrange:=46;橙];[CBlueGray:=47;蓝灰];[CDarkCyan:=49;深青];[CMiddleGreen:=50;中绿];[CTan:=53;褐];[CDarkBlue:=55;黑蓝];[CAuto:=-4105;自动];[CNone:=-4142;无色]
;global ExcelStatic
;    excel:=Excel_Get()
;    Selection:=excel.Selection
;    Selection.Interior.ColorIndex:=ExcelStatic.CYellow
;MyColorArray:={"r":3,"R":22,"b":33,"B":28,"y":6,"Y":36,"g":4,"G":35,"o":45,"O":40,"g1":15,"g2":48,"g3":16,"g4":56,"w":2,"k":1,"a":-4105,"n":-4142}
;Excel_指定填充/字体颜色【快捷键小写】（区别在于：MyValue:=MyColorArray1[MyIndex]）
MyColorArray1:={"r":3,"b":33,"y":6,"g":4,"o":45,"g1":15,"g2":48,"g3":16,"g4":56,"w":2,"k":1,"a":-4105,"n":-4142}
MyColorArray2:={"R":22,"B":28,"Y":36,"G":35,"O":40}
global VimD
MyType:=substr(VimD.HotKeyStr,1,1)
MyIndex:=substr(VimD.HotKeyStr,2)
if ((MyIndex+0)>0)
	MyIndex=g%MyIndex%
MyValue:=MyColorArray1[MyIndex]
excel:=Excel_Get()
Selection:=excel.Selection
try{
if (MyType="r")		;填充颜色
	Selection.Interior.ColorIndex:=MyValue
if (MyType="t")		;字体颜色
	Selection.font.ColorIndex:=MyValue
}
Return
2C0F563D-BBEB-45F2-A9CC-42B678C082C7:
;&G  LICECap直接开始录制GIF
run apps\LICECap\licecap.exe
sleep 100
send {Space}
sleep 500
clipboard = LiceCap屏幕录制_%A_now% ;增加上面这句，把当前的系统日期发送到剪贴板
Send, ^v{Enter} ;发送 Ctrl + v 和回车
return
Return
2C345DA6-D9F1-4B12-8720-8F72A7B43F3B:
;Excel_在头部输入
    excel:=Excel_Get()
    sendinput,{f2}
    sendinput,{home}
Return
2C584D47-FC21-45FA-8CB5-5506218B9DBC:
;Excel_内部垂直方向-点线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Selection.Borders(ExcelStatic.xlInsideVertical).LineStyle := ExcelStatic.xlDot
    Selection.Borders(ExcelStatic.xlInsideVertical).Weight := ExcelStatic.xlHairline
}
Return
2E1A1729-0DFB-4023-90B7-4EE7D13CB1C2:
;Phantom_页面管理-旋转-从到-180
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
sendinput,{down}
;sendinput,{enter}
Return
2E355C3B-FA43-4B59-AE85-C349AB5315C7:
;Excel_筛选-不包含
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("<>*", ActiveCell.Value "*" ,"fe")
Return
2F2699C1-C7BA-463C-B658-C8AD97795AA6:
;Excel_设置/取消斜体
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    If (Selection.Font.Italic)
        Selection.Font.Italic := False
    Else
        Selection.Font.Italic := True
}
Return
2F5C5241-18C3-4F69-B365-0D7C88B043B9:
;Phantom_关闭当前标签
sendinput,^w
Return
2F8E5F3D-4F30-4247-9E53-859BCC9F9A51:
;Excel_筛选-开头是
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", ActiveCell.Value "*" ,"fB")
Return
2FA3FEC6-8CF9-4BEE-9088-AF8A121CFBCB:
;&J  智能解压
 GetFiles:=QZData("{file:path}")
 RunCmd=%A_ScriptDir%\Apps\Kawvin系列软件\Kawvin智能解压\Kawvin智能解压.exe "%GetFiles%" 
runwait,%RunCmd%
Return
30200A71-C4B8-4F9E-9A63-B325B1F060D8:
;Excel_筛选-包含
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=*", ActiveCell.Value "*" ,"fi")
Return
3100E2AD-81B3-44F0-B26C-A45122AD9A46:
;Excel_以当前单元格的值设置页脚右侧
 try{    
   excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.RightFooter := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
33267AA5-B30D-4B94-B6B0-345A3EAB0644:
;Excel_筛选-降序排列
    FilteredRowColon:=
    FilteredRow:=
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
try{
    FilteredRowColon := strSplit(ActiveSheet.AutoFilter.Range.Rows(1).Address, "$")(3)
    FilteredRow := strSplit(FilteredRowColon, ":")(1)
    excel.Cells(FilteredRow, ActiveCell.Column).Activate
    Sendinput,!{DOWN}
    sleep,30
    sendinput,o
}
Return
33C74D13-6EEE-4441-8AC2-FAF40C819FB2:
;&S  所选文本转到 SciTE4 中编辑
AppPath:=QZData("%scite%")
clipboard:=QZData("{Text}" )
ifnotexist,%AppPath%
	return
IfWinNotExist	ahk_class SciTEWindow	;如果SciTE4没有打开
{
	Run,%AppPath%,,Max
	WinWait,ahk_class SciTEWindow,,5
	if ErrorLevel		;如果5秒没激活，则退出
		return
	WinActivate
	sendinput,^v
}
IfWinNotActive ahk_class SciTEWindow
{
	Run,%AppPath%,,Max
	WinWait,ahk_class SciTEWindow,,5
	if ErrorLevel			;如果5秒没激活，则退出
		return
	WinActivate
	sendinput,^n
	sleep,300
	sendinput,^v
}
clipboard:=
Return
348BB786-CA6B-4D45-BC4A-4BC89633D79C:
;Excel_筛选-不相同的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("<>", ActiveCell.Value,"fn")
Return
34D2CE1C-3917-4A0D-AD77-C30FB231A7C8:
;Excel_字号变小
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    currentFontSize := Selection.Font.Size
    Selection.Font.Size := currentFontSize - 1
}
Return
3506C66E-E5A2-4180-8186-F273B9F4DDA0:
;Excel_定位到当前区域边缘-下
sendinput,^{down}
Return
358C0382-7C25-4F39-B975-04A9D4E3900A:
;Excel_边框-全部细实线
	excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlInsideHorizontal).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlInsideHorizontal).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlInsideVertical).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlInsideVertical).Weight := ExcelStatic.xlThin
	Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeTop).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeBottom).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeRight).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeLeft).Weight := ExcelStatic.xlThin
}
Return
3593D660-F061-46A2-97DF-7CD53A1AB1BC:
;Excel_筛选-升序排列
    FilteredRowColon:=
    FilteredRow:=
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
try{
    FilteredRowColon := strSplit(ActiveSheet.AutoFilter.Range.Rows(1).Address, "$")(3)
    FilteredRow := strSplit(FilteredRowColon, ":")(1)
    excel.Cells(FilteredRow, ActiveCell.Column).Activate
    Sendinput,!{DOWN}
    sleep,30
    sendinput,s
}
Return
35AB316B-C214-44DA-9918-6B98F2B7BE23:
;&D  转到 Notepad++ 中编辑
Editor= ;执行前清空变量
Editor:=QZData("%NotepadJJ%") ;在本行指定要使用的编辑器变量
;===========
;以下内容无需改动
;===========
dPath=
dPath:=getDocumentPath()
WinActivate % "ahk_class" QZData("winclass")
WinWaitActive % "ahk_class" QZData("winclass")
dTitle=
WinGetTitle, dTitle,  A
;~ ;===========
;~ ;通过Pipe运行代码
;~ ;===========
ScriptContent:=% "SetTitleMatchMode, 3`nGroupAdd, GroupName, " dTitle "`nWinClose, ahk_group GroupName`nWinWaitClose, ahk_group GroupName`nRun, """ Editor """ """ dPath """`nreturn"
CustomFunc_PipeRun(ScriptContent)
Return
Return
35EBA71C-0E75-46CE-8043-FA747E0F3D20:
;Excel_筛选-与所选单元格前1-8个字符相同
    buff_string:="f8"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
371859D4-F66E-4284-816B-3855E17CE9F6:
;Excel_另存Excel文件
    sendinput,^+s
Return
392BD217-5A89-41D7-B15A-014EA4166599:
;TC_全选
Postmessage, 1075, 3301, 0,, ahk_class TTOTAL_CMD
Return
39E1241F-422A-488B-8AA1-FCA331919CBE:
;Excel_以当前单元格的值设置页脚左侧
try{    
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.LeftFooter := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
3A45BF88-F3D0-4283-B6F2-18F1B142EDD5:
;&T  重命名 替换文件名中指定内容(Shift:编辑 Ctrl:临时)
ReplaceList=%A_ScriptDir%\User\替换列表.txt
IfNotExist,%ReplaceList%
	return

MyKeyStatus:=
if GetKeyState("Ctrl")
	MyKeyStatus.="Ctrl"
if GetKeyState("Shift")
	MyKeyStatus.="Shift"
if GetKeyState("Alt")
	MyKeyStatus.="Alt"
if GetKeyState("Win")
	MyKeyStatus.="Win"
if GetKeyState("CapsLock", "T")
	MyKeyStatus.="Caps"
;~ msgbox , % MyKeyStatus

if (instr(MyKeyStatus,"Shift")>0) ;按下Shift选择菜单，则编辑替换列表
{
	Run,%ReplaceList%
	return
}
MyArray_List:={}
if (instr(MyKeyStatus,"Ctrl")>0)   ;按下Ctrl选择菜单，则临时替换
{
	InputBox, MyInput, `n提示,请输入替换及替换内容，格式：xxx`=yyy`n`n如果格式为 xxx`=yyy`|x，则添加到替换列表，x可以为任意字符
	StringSplit,MyArray_List,MyInput,`|
	if MyArray_List2!=   ;如果格式为 xxx`=yyy`|x，则添加到替换列表
		FileAppend,`n%MyArray_List1%`n,%ReplaceList%
}
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,,MyOutDir,MyOutExt,MyOutNameNoExt
	MyNewFile=%MyOutDir%\%MyOutNameNoExt%
	;If InStr(FileExist(MyTemFile), "D")
	;    continue
	if MyArray_List1!=   ;如果是临时替换
	{
		MyOldStr:=RegExReplace(MyArray_List1,"=.*?$")  	 ;要替换的字符串
		MyNewStr:=RegExReplace(MyArray_List1,"^.*?=") 	;替换为的字符串
		StringReplace,MyNewFile,MyNewFile,%MyOldStr%,%MyNewStr%,all
	} else {
		loop,Read,%ReplaceList%,`n
		{
			if Trim(A_LoopReadLine)=""
				continue
			MyOldStr:=RegExReplace(A_LoopReadLine,"=.*?$")  	 ;要替换的字符串
			MyNewStr:=RegExReplace(A_LoopReadLine,"^.*?=") 	;替换为的字符串
			StringReplace,MyNewFile,MyNewFile,%MyOldStr%,%MyNewStr%,all
		}
	}
	if MyTemFile=%MyNewFile%.%MyOutExt%
		continue
	MyNewFileCount=%MyNewFile%.%MyOutExt%
	if MyNewFileCount=
		return
	loop
	{
		IfNotExist,%MyNewFileCount%
			break
		MyNewFileCount=%MyNewFile%-%A_Index%.%MyOutExt%
	}
	if InStr(FileExist(MyTemFile), "D")
		FileMoveDir,%MyTemFile%,%MyNewFileCount%
	else
		FileMove,%MyTemFile%,%MyNewFileCount%
}
Return
3A941408-39D4-4EE8-800C-FB39A21491B0:
;&P  复制完整路径
clipboard := QZData("Files")
Return
3AC67716-35B0-443A-AE77-C8E735303EBF:
;&Z 在资源管理器打开
ExplorerPath := QZData("text")
run, %ExplorerPath%
return
Return
3B932E87-A107-4A73-9C29-CCD3CCDB2979:
;Excel_边框-无边框
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := False
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := False
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := False
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := False
    Selection.Borders(ExcelStatic.xlInsideVertical).LineStyle := False
    Selection.Borders(ExcelStatic.xlInsideHorizontal).LineStyle := False
}
Return
3BF3244A-36FF-4961-BDAE-A397B4A05774:
;&Q  和此QQ好友聊天
clipboard := QZData("text")
;If ! (RegExMatch(Clipboard,"^[0-9]{5,11}$"))  ;限定qq的位数为5--11位
;return
;else
run,tencent://message/?uin=%clipboard%&fuin=105224583
;uin是要发起的对象，fuin是谁要发起,如果电脑登录了一个以上的qq的话加上fuin可以控制你要发起的那个qq号，如果电脑只登陆了一个qq，fuin参数可以去掉
return
Return
3C43F296-A6EF-461C-A195-AE66B3957D9A:
;Excel_当前工作表移动到右侧
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveSheet.Select
try{
    If (excel.ActiveSheet.index = excel.Worksheets.Count )
        excel.ActiveSheet.Move(Before:=excel.sheets(1))
    Else 
        excel.ActiveSheet.Move(,After:=excel.sheets(excel.ActiveSheet.index + 1))
}
Return
3CC2BA0E-969C-4021-9165-9688E6716BD8:
;Excel_筛选-与所选单元格后1个字符相同
    buff_string:="F1"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
3CC71DE3-1F1C-4E88-BDF6-680EA3DDF678:
;Excel_筛选-相同的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", ActiveCell.value,"fs")
Return
3ED0C821-7A7B-40FB-988E-09E5B6D8D6C9:
;Excel_删除单元格-下方单元格上移
    sendinput,{AppsKey}
    sleep,30
    sendinput,d
    sleep,30
    sendinput,!u
    sleep,30
    sendinput,{enter}
Return
3FA8838E-972A-4A53-B08A-529C8D30272E:
;Phantom_页面管理-旋转-当前页-180
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
sendinput,{down}
sendinput,{enter}
controlclick,Button7,,旋转页面,,
Return
4058D0F0-8D63-4AE0-BCE1-377B2736FED5:
;&T  打开当前目录(TotalCMD)
;获取当前目录
CurWinClass:=QZData("winclass") ;将获取的class名赋值给用户变量
Curhwnd:=QZData("hWnd")
if (CurWinClass="ExploreWClass") or (CurWinClass="CabinetWClass") ;如果当前激活窗口为资源管理器
{
	DirectionDir:=Explorer_GetPath_Kawvin1(Curhwnd)
	IfInString,DirectionDir,`;		;我的电脑、回收站、控制面板等退出
		return
}
if (CurWinClass="WorkerW") or (CurWinClass="Progman") ;如果当前激活窗口为桌面
{
	;DirectionDir=%A_Desktop%
	DirectionDir:=Explorer_GetPath_Kawvin1(Curhwnd)
}
if (CurWinClass="Shell_TrayWnd") ;如果当前激活窗口为任务栏
	return
If(DirectionDir="ERROR")		;错误则退出
	return
;在TC中打开
TLAppPath:=QZData("%TOTALCMD%")
ifnotexist,%TLAppPath%
	return
IfWinNotExist	ahk_class TTOTAL_CMD
	Run,%TLAppPath%,,Max
IfWinNotActive ahk_class TTOTAL_CMD
{
	Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
	WinWait,ahk_class TTOTAL_CMD
	WinActivate
}
PostMessage 1075, 3001, 0, , AHK_CLASS TTOTAL_CMD
ControlSetText, Edit1, cd %DirectionDir%, ahk_class TTOTAL_CMD
Sleep,400
ControlSend, Edit1, {Enter}, ahk_class TTOTAL_CMD
return

Explorer_GetPath_Kawvin1(hwnd="")
{
	if !(window := Explorer_GetWindow_Kawvin1(hwnd))
		return ErrorLevel := "ERROR"
	if (window="desktop")
		return A_Desktop
	path := window.LocationURL
	path := RegExReplace(path, "ftp://.*@","ftp://")
	StringReplace, path, path, file:///
	StringReplace, path, path, /, \, All
	loop
		if RegExMatch(path, "i)(?<=%)[\da-f]{1,2}", hex)
			StringReplace, path, path, `%%hex%, % Chr("0x" . hex), All
		else break
	return path
}

Explorer_GetWindow_Kawvin1(hwnd="")
{
	WinGet, Process, ProcessName, % "ahk_id" hwnd := hwnd? hwnd:WinExist("A")
	WinGetClass class, ahk_id %hwnd%

	if (Process!="explorer.exe")
		return
	if (class ~= "(Cabinet|Explore)WClass")
	{
		for window in ComObjCreate("Shell.Application").Windows
			if (window.hwnd==hwnd)
				return window
	}
	else if (class ~= "Progman|WorkerW")
		return "desktop" ; desktop found
}
Return
421B4460-8697-4BE2-83A3-1CD1AB0D3A3B:
;TC_返回上级目录
Postmessage, 1075, 2002,0,, ahk_class TTOTAL_CMD
Return
422655D8-FECB-4F36-94FA-5E59F679B59A:
;通用_查找（Ctrl+F）
    sendinput,^f
Return
431641C9-B12C-4B97-95AB-E60C3F6F1EBB:
;Excel_筛选-数值大于（>）的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter(">", ActiveCell.Value,"f>")
Return
45C7C344-035C-4CD1-8071-2BAC6AA3F3B7:
;Phantom_页面管理-插入页面-从文件
WinMenuSelectItem, A,, 页面管理,插入页面,从文件
sendinput,{tab}
sleep,50
sendinput,{enter}
Return
46C845D3-ACD1-4828-9EC0-84E6334A48B9:
;测试用_全局热键
msgbox ,显示本消息说明，全局热键成功
Return
47156376-1C70-4506-AE7E-1D9CF661F7C8:
;Excel_定位到当前区域（下）单元格
sendinput,^{down}
sendinput,{down}
Return
47F9B4F9-2095-4C2E-A22D-7133604839DA:
;&U  文件、文件夹移动到上级文件夹
MySel:=QZData("Files")
;msgbox % MySel
MyParentDir:=
MyGrandDir:=
loop,Parse,% MySel,`n,`r    ;循环读取每一行
{
	MyTemFile := trim(A_loopfield)
	if (A_index=1)
	{
		SplitPath,MyTemFile,,MyParentDir		;所选文件的父目录
		SplitPath,MyParentDir,,MyGrandDir		;父目录的上级目录
	}
	if InStr(FileExist(MyTemFile), "D")  ;区分是否文件夹,Attrib= D ,则是文件夹
	{
		SplitPath,MyTemFile,SourDir
		FileMoveDir, %MyTemFile%, %MyGrandDir%\%SourDir%
	} else {
		FileMove,%MyTemFile%,%MyGrandDir%
	}
}
Return
482EE792-DC8B-4A88-B210-A8DBE586D324:
;Excel_筛选-与所选单元格后1-2个字符相同
    buff_string:="F2"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
48BC9C2F-DA59-4E77-B013-1EAC17E64444:
;Phantom_页面管理-旋转-全部-180
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
sendinput,{down}
sendinput,{tab}
sendinput,{up}
sendinput,{up}
sendinput,{up}
sendinput,{enter}
Return
49DDF6AC-3B59-4D73-BE01-1A6CEBB2C3B3:
;标签测试（示例）
;文件标签
msgbox % QZData( "Files" )
msgbox % QZData( "FileExt" )
msgbox % QZData( "FileName" )
msgbox % QZData( "FileDir" )
msgbox % QZData( "FileMulti" )
msgbox % QZData( "{file:path}" )
;变量
msgbox % QZData( "%Photoshop%" )
;文本
msgbox % QZData( "{text}" )
msgbox % QZData( "{text:cp0}" )
msgbox % QZData( "{text:utf-8}" )
msgbox % QZData( "{select}" )
;日期时间
msgbox % QZData( "{date:yyyyMMdd}" )
msgbox % QZData( "{date:yyyy-MM-dd}" )
msgbox % QZData( "{date:yyyy.MM.dd}" )
msgbox % QZData( "{date:yyyyMMdd_HHmmss}" )
msgbox % QZData( "{date:yyyy年MM月dd日HH时mm分}" )
msgbox % QZData( "{date:yyyy年MM月dd日_HH时mm分ss秒}" )
;输入对话框
msgbox % QZData( "{input:1234}" )
msgbox % QZData( "{input:44444:hide}" )
;文件对话框
msgbox % QZData( "{box:filebrowser}" )
msgbox % QZData( "{box:folderbrowser}" )
;列表框
msgbox % QZData( "{box:list[www.163.com][114.114.114.114][www.baidu.com]}")

Return
49DF1DFF-23B0-4A6A-A93B-18C555C9A0D0:
;Excel_筛选-与所选单元格前1-3个字符相同
    buff_string:="f3"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
4AC70C9E-D885-4E8C-9F0B-7F69FBAC5B37:
;Excel_运行宏-临时4
sendinput,{Enter}
;comObjactive("excel.application").run("Personal.xlsb!公式类_2粘贴数据公式")
excel:=Excel_Get()
excel.Cells(1,6).select
Return
4BB71AF0-AA9C-44CF-8203-9648A104B5DD:
;Excel_排序窗口
;sendinput,!a
;sleep,300
;sendinput,!    ;分开写，避免与其他截图类软件冲突
;sleep,300
;sendinput,a
;sleep,300
;sendinput,ss
global ExcelStatic
excel:=Excel_Get()
try
excel.Dialogs(ExcelStatic.xlDialogSort).Show
Return
4BC61A98-394B-45B1-AD2A-26852AF394B9:
;Excel_对齐-水平居中
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.HorizontalAlignment := ExcelStatic.xlHAlignCenter
Return
4D085B03-FACA-4A54-BB24-7048EDB273DE:
;TC_测试Ctrl+1
FilesArray:={}
QZ_GetClip(FilesArray)
msgbox % FilesArray.files
msgbox % FilesArray.text

ExeArray:={}
QZ_GetWinInfo(ExeArray)
msgbox % ExeArray.x
msgbox % ExeArray.y
msgbox % ExeArray.HWND
msgbox % ExeArray.WinClass
msgbox % ExeArray.WinExe
msgbox % ExeArray.WinControl
msgbox % ExeArray.WinTitle
Return
4E0A0660-E9E1-41DF-8759-5352511AB2CD:
;通用_返回（Esc）
sendinput,{esc}
Return
4E0F8C27-1825-4C43-B07F-AD48C00D0BB7:
;Excel_百分号
    excel:=Excel_Get()
    sendinput,!h
    sleep,30
    sendinput,p
    ;Selection:=excel.Selection
    ;Selection.Style := "Percent"
Return
4E5A1B4F-E53E-401C-B56F-8A1EFA43817E:
;Excel_行高减小
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    currentHeight := excel.Cells(Selection.Row, Selection.Row).RowHeight
    Selection.RowHeight := currentHeight - 1
}
Return
4F511E1C-4527-4122-87E7-696E116EBBA2:
;Excel_筛选-与所选单元格后1-8个字符相同
    buff_string:="F8"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
4FC8D350-FC32-45DF-9F6B-3076914C55DC:
;Excel_退出Excel
    excel:=Excel_Get()
try
    excel.ActiveWorkbook.Close
Return
505570AD-D286-4962-A360-F27A0F5D3814:
;Excel_跨列对齐
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.HorizontalAlignment := ExcelStatic.xlHAlignCenterAcrossSelection
Return
52783379-56CB-471B-85CC-2ABCE5F9747B:
;切换到【普通模式】

vimd.changemode("普通模式")
Return
54568C84-BF05-44E2-BB25-257EA7E55386:
;TC_切换到【普通模式】
send,{esc}
vimd.changemode("普通模式")
MsgBox, 0, 提示, 【普通模式】, 0.3
Return
54784AC1-3E96-454A-B8A0-2881CC22D1D2:
;Excel_工作表循环-定义原始工作表
global LastSelectSheetIndex
excel:=Excel_Get()
ActiveSheet:=excel.ActiveSheet
try
	LastSelectSheetIndex:=excel.ActiveSheet.index
catch e
    LastSelectSheetIndex:=1
Return
549CE6A1-1F37-48A6-81D5-C258A0098A2F:
;Excel_运行宏-临时1
try
comObjactive("excel.application").run("Personal.xlsb!公式类_1复制数据公式")
Return
54C2B612-10BC-49D7-9A33-432818266A3F:
;Excel_对齐-靠下
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.VerticalAlignment := ExcelStatic.xlVAlignBottom
Return
55891D59-6019-4CD8-B045-5A6870964AE8:
;Excel_定位列首个单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.Cells(1, ActiveCell.Column).Select
Return
56121F61-64B4-4151-9146-DB37957F9A9B:
;Phantom_文件-打印从到
InputBox, NewString, 输入, 请输入页码范围`n如：1-4或2`,4-8, , 300,150
if ErrorLevel
	return
if (NewString="")
	return
else
{
	sendinput,^p
	sleep,300
	sendinput,!e
	sleep,100
	ControlSetText, Edit3, %NewString%, , 打印
	sleep,300
	sendinput,{enter}
}
Return
56469B26-129C-4776-BBD8-63323674E904:
;Excel_字体颜色-红
;[CBlack:=1;黑];[CWhite:=2;白];[CRed:=3;红];[CGreen:=4;绿];[CBlue:=5;蓝];[CYellow:=6;黄];[CMagenta:=7;粉红];[CCyan:=8;青];[CGray25:=15;灰色25];[CGray40:=48;灰色40];[CGray50:=16;灰色50];[CPurple:=17;紫];[CLightRed:=22;浅红];[CSkyBlue:=33;天蓝];[CCyanGreen:=34;浅青绿];[CLightGreen:=35;浅绿];[CLightYellow:=36;浅黄];[CLightBlue:=37;浅蓝];[CRoseRed:=38;玫瑰红];[CLigthPurple:=39;浅紫];[COrangeBrown:=40;茶色];[CMiddleBlue:=41;浅蓝];[CWaterGreen:=42;水绿色];[CGrassGreen:=43;青草绿];[CGold:=44;金];[CLightOrange:=45;浅橙];[COrange:=46;橙];[CBlueGray:=47;蓝灰];[CDarkCyan:=49;深青];[CMiddleGreen:=50;中绿];[CTan:=53;褐];[CDarkBlue:=55;黑蓝];[CAuto:=-4105;自动];[CNone:=-4142;无色]
global ExcelStatic
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.Font.ColorIndex:=ExcelStatic.CRed
Return
568FB290-4E04-445C-9FE3-8FFBE1ED98E2:
;Excel_取消缩进对齐
    excel:=Excel_Get()
    Selection:=excel.Selection
    try
        Selection.InsertIndent(-1)
Return
5723A1AE-AAFC-4A10-ADD3-FA1B0EC2D1F1:
;&T  打开快捷方式所指目录(TotalCMD)
MySel:=QZData("files" )
TLAppPath:=QZData("%TOTALCMD%")
FileGetShortcut,%MySel%,MyTemFile
SplitPath,MyTemFile,,MyOutDir
Run,%TLAppPath% /T /R="%MyOutDir%"
Return
586BED51-52E0-46E0-8637-FB7A47CB1E4B:
;Excel_筛选-与所选单元格前1-2个字符相同
    buff_string:="f2"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
588F9E52-B4B3-46A1-8E78-810FBC89BF36:
;Excel_筛选-数值小于（<）的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("<", ActiveCell.Value,"f<")
Return
58C38C2B-0717-4F3E-9622-C919389C2DF1:
;Excel_自动筛选
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    Selection:=excel.Selection
try{
    If (ActiveSheet.AutoFilterMode)
       ActiveSheet.AutoFilterMode := False
    Else
       Selection.AutoFilter
}
Return
5931BB1C-59C7-44D3-B451-77BF3A674E78:
;Excel_筛选-与所选单元格后1-4个字符相同
    buff_string:="F4"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
5ABD9A82-2D4F-4D15-BFB5-AD4C21B87DA3:
;Phantom_页面管理-旋转-当前页-逆90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{enter}
controlclick,Button7,,旋转页面,,
Return
5B8EF5E2-1FC3-4E88-9289-ECE41FF6D186:
;通用_删除（Del）
    SendInput,{del}
Return
5DD81267-1361-4723-9BB9-CC14C4462706:
;Excel_以当前单元格的值设置页眉右侧
    try{    
excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.RightHeader := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
5E0ECBFD-3C81-49CB-9286-ADB2957443D8:
;&C  文件、文件夹复制到(...)
MySel:=QZData("files")
FileSelectFolder,MySelDir,*,3
if (MySelDir="")
	return
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile, SourceFolderName ; 仅从它的完全路径提取文件夹名称。
	if InStr(FileExist(MyTemFile), "D")  ;区分是否文件夹,Attrib= D ,则是文件夹
		FileCopyDir, %MyTemFile%, %MySelDir%\%SourceFolderName%、
	else
		FileCopy,%MyTemFile%,%MySelDir%
}
Return
5E71C897-D541-4533-873E-4D8FDD708997:
;Excel_运行宏-临时2
try
comObjactive("excel.application").run("Personal.xlsb!公式类_2粘贴数据公式")
Return
5FE0A3A1-35AD-442F-99BD-BB255C63DD90:
;Phantom_视图-适合页面
;send,!v
;send,p
;send,s
WinMenuSelectItem, A, , 视图, 页面布局,单面
;send,^2
WinMenuSelectItem, A, , 视图, 缩放,适合页面
Return
60826C34-552E-4986-B1B5-F8A851EF17E4:
;Excel_删除选中列
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.EntireColumn.Delete
Return
60BC6DFB-0669-4431-B14A-8E9719A1994A:
;Excel_选择到当前区域边缘-右
sendinput,^+{right}
Return
611A7A8C-2D1E-42D0-BE78-8C7BC6B3CAAA:
;&F  扫描图标保存到文件夹
FileSelectFolder, SaveDir, *, 3，请选择要保存图标的目录
if SaveDir!=
{
    if (substr(SaveDir,-1)!="\")
        SaveDir=%SaveDir%\
    GetFiles:=QZData("{file:path}")
    ;GetFiles:=QZData("files")
    RunCmd=Apps\iconsext\iconsext.exe /Save  "%GetFiles%"  "%SaveDir%"  -icons
   ;msgbox % RunCmd
   run ,% RunCmd
}
Return
623A6090-5F1E-488F-93BD-95A400F6ABCE:
;Excel_查找下一项
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try{
    Rng:=excel.Cells.findNext(After:=ActiveCell)
    if IsObject(Rng)
        Rng.Activate
}
Return
645B6E59-CE2D-4848-BC93-F9A82A1CF0CC:
;Excel_在页眉右侧设置当前日期
    excel:=Excel_Get()
try
    excel.ActiveSheet.PageSetup.RightHeader := "&D"
Return
650588FE-25CB-44DB-B614-D1F3D723CC95:
;Excel_插入行-当前单元格下方
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row+1,ActiveCell.column).select
    sendinput,{AppsKey}
    sleep,30
    sendinput,i
    sleep,30
    sendinput,!r
    sleep,30
    sendinput,{enter}
Return
659FCC1E-80F4-48B3-A1DD-C6A63CDE2872:
;Excel_粘贴为数值
    sendinput,^!v
    sleep,50
    sendinput,v
    sleep,50
    sendinput,{enter}
Return
66034F3F-B917-4576-A0B3-BA531D93C09C:
;Excel_删除单元格-右侧单元格左移
    sendinput,{AppsKey}
    sleep,30
    sendinput,d
    sleep,30
    sendinput,!l
    sleep,30
    sendinput,{enter}
Return
672C6B08-88AB-4BE3-B2D7-4383F9FF1A09:
;Phantom_页面管理-提取页面-从到
;sendinput,!o
;sendinput,e
WinMenuSelectItem, A, , 页面管理, 提取页面
sleep,300
sendinput,s
sleep,100
sendinput,{space}
Return
6735DAFE-55FA-457D-8CC2-D8CFB8A4EA16:
;通用_关闭（Alt+F4）
sendinput,!{f4}
Return
67A36BEC-D1A9-49F9-981F-C2E62E3F6DA4:
;Phantom_编辑-菜单选择标签内容
global CurID
global PageStr

MouseGetPos,CurX,CurY
menu,RMenu,Add
menu,RMenu,DeleteAll
loop,% PageStr.MaxIndex()
	menu,RMenu,add,% PageStr[A_index],MySub_AddPDFLebal
menu,RMenu,show,%CurX%,%CurY%
return

MySub_AddPDFLebal:
	;sendinput,!e
	;sendinput,d
WinMenuSelectItem, A, , 编辑, 添加书签
	sleep,300
	sendinput,%A_ThisMenuItem%
	sleep,100
	sendinput,{enter}
	sleep,100
	mouseclick,,500,500
	sleep,300
	sendinput,{PgDn}
return
Return
68378E41-2778-4BAE-B8DC-8A1B21C1B32B:
;&C  Unicode码转为中文
QZSel:=CustomFunc_QZSel()
Text:=QZSel.text
Text:=CustomFunc_uXXXX2CN(Text)
/*

While pos := RegExMatch(text, "\\u\w{4}")
{
  b := SubStr(text, pos+2, 4)
  b := uXXXX2CN(b)
  text := RegExReplace(text, "\\u\w{4}", b, "", 1)
}
*/

MsgBox, 262145, 转换结果, %text%`n`n======`n`n确定：复制到剪贴板`n取消：直接退出
ifmsgbox, yes
	Clipboard:=Text

return
Return
690EE1C6-B027-4AAF-9254-74852801AE95:
;TC_移动到另一窗口
Postmessage, 1075,1005, 0,, ahk_class TTOTAL_CMD
sleep,300
sendinput,{Enter}
Return
69ECF897-17E2-4D97-9F40-4A5B6099FA85:
;Excel_对齐-垂直居中
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.VerticalAlignment := ExcelStatic.xlVAlignCenter
Return
6A893A49-6D26-4D4C-81E8-201D01707A99:
;Excel_数字格式-减少小数位
    sendinput,!h
    sleep,30
    sendinput,9
Return
6ABFEFBF-F892-4A5E-B1F1-C0E719571074:
;&F  使用Foxit_Phantom批量合并文件
MySel:=QZData( "Files" )
run "%A_ahkpath%" "%A_ScriptDir%\Apps\使用Foxit_Phantom批量合并文件.ahk" "%MySel%"
Return
6E1B4536-C2C0-40F7-A775-95212189C487:
;切换到【VIM模式】

vimd.changemode("VIM模式")
Return
6E877D15-DF9F-40ED-A9D5-A568E87572FE:
;Excel_填充颜色-黄
;[CBlack:=1;黑];[CWhite:=2;白];[CRed:=3;红];[CGreen:=4;绿];[CBlue:=5;蓝];[CYellow:=6;黄];[CMagenta:=7;粉红];[CCyan:=8;青];[CGray25:=15;灰色25];[CGray40:=48;灰色40];[CGray50:=16;灰色50];[CPurple:=17;紫];[CLightRed:=22;浅红];[CSkyBlue:=33;天蓝];[CCyanGreen:=34;浅青绿];[CLightGreen:=35;浅绿];[CLightYellow:=36;浅黄];[CLightBlue:=37;浅蓝];[CRoseRed:=38;玫瑰红];[CLigthPurple:=39;浅紫];[COrangeBrown:=40;茶色];[CMiddleBlue:=41;浅蓝];[CWaterGreen:=42;水绿色];[CGrassGreen:=43;青草绿];[CGold:=44;金];[CLightOrange:=45;浅橙];[COrange:=46;橙];[CBlueGray:=47;蓝灰];[CDarkCyan:=49;深青];[CMiddleGreen:=50;中绿];[CTan:=53;褐];[CDarkBlue:=55;黑蓝];[CAuto:=-4105;自动];[CNone:=-4142;无色]
global ExcelStatic
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.Interior.ColorIndex:=ExcelStatic.CYellow
Return
6F7624DB-76E8-4BEE-9285-2CC9204B18C3:
;TC_复制到另一窗口
Postmessage, 1075, 3101, 0,, ahk_class TTOTAL_CMD
sleep,300
sendinput,{Enter}
Return
6F77EA5F-96BA-435B-9D2E-E43D8033FACF:
;Excel_定位到当前区域边缘-左
sendinput,^{left}
Return
70647EBA-42D6-45B4-BC8B-3700D500523E:
;TC_全不选
Postmessage, 1075, 524, 0,, ahk_class TTOTAL_CMD
Return
72016A49-847B-4134-8A97-8CEF0DA0F2C1:
;Excel_插入行-当前单元格上方
    sendinput,{AppsKey}
    sleep,30
    sendinput,i
    sleep,30
    sendinput,!r
    sleep,30
    sendinput,{enter}
Return
72161276-EE4F-48DD-8903-D31136B4521C:
;Excel_向下添加选择
    sendinput,+{down}
Return
72238DFD-E3F2-44BC-A403-8C98FB3BAD11:
;Phantom_页面管理-删除当前页
;send !o
;send d
WinMenuSelectItem, A, , 页面管理, 删除页面
send o
send {enter}
Return
72CE4F31-9A7E-489B-AC87-024D0220F52B:
;Excel_保存并退出Excel
    global ExcelStatic
excel:=Excel_Get()
try{    
    excel.Dialogs(ExcelStatic.xlDialogSummaryInfo).Show
    If !(excelActiveWorkbook.Saved)
        sendinput,^s
    excel.ActiveWorkbook.Close
}
Return
74FCCEFF-0210-4B7E-B0C4-81F7B76919AE:
;Phantom_页面管理-提取页面-当前
;sendinput,!o
;sendinput,e
WinMenuSelectItem, A, , 页面管理, 提取页面
sleep,300
sendinput,s
sleep,100
sendinput,{space}
sendinput,{enter}
Return
750E6679-56E2-40CD-B701-CAFBE7856543:
;Excel_选中列-显示
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.EntireColumn.Hidden := False
Return
75E74C37-2F6F-47D7-A9C6-4104B1CF1033:
;通用_剪贴（Ctrl+X）
    sendinput,^x
Return
75ED0BBB-D941-4C0E-B039-607062CDBFA9:
;Excel_删除行-从当前行到底部
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_rows_string:=bottomR . ":65536"
    Selected_range :=  excel.ActiveSheet.Range(Selected_rows_string)
    Selected_range.delete
}
Return
765E2B22-50A3-4639-A45D-FFD876B79DB9:
;Excel_选择到当前区域边缘-上
sendinput,^+{up}
Return
76F8BCF8-BBDC-48FA-B8C0-E1E1397DB862:
;Phantom_文件-打印全部
sendinput,^p
sleep,300
sendinput,{enter}
Return
77411D4A-692B-4ACE-A26F-D7AEBDB9F07C:
;通用_确认（Enter）
sendinput,{Enter}
Return
776D15F6-716A-4C67-9FE7-779FFED2AFFD:
;&3  重命名 (输入内容)文件、文件夹名
InputBox, MyInsert, 提示,请输入要添加的内容
if (MyInsert="")
	return
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	if InStr(FileExist(MyTemFile), "D")
	{
		MyNewFile =%MyOutDir%\%MyInsert%%MyOutNameNoExt%
		FileMoveDir,%MyTemFile%,%MyNewFile%
	} else {
		MyNewFile =%MyOutDir%\%MyInsert%%MyOutNameNoExt%.%MyOutExt%
		FileMove,%MyTemFile%,%MyNewFile%
	}
}
Return
78844689-99D4-4CF2-968D-C2D6FE001427:
;Excel_在页脚设置[x/x]页码
    excel:=Excel_Get()
try
    excel.ActiveSheet.PageSetup.CenterFooter := "&P/&N"
Return
7897BCDF-1615-4A20-A10C-DD1F28C122CB:
;Excel_切换到前个工作表（可循环）
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
try{    
    If (ActiveSheet.index = 1)
        excel.Worksheets(excel.Worksheets.Count).Select
    Else
        ActiveSheet.Previous.Select
}
Return
79F3E01D-6964-4C43-9C0C-36E91A68775B:
;Excel_查找上一项
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try{
    Rng:=excel.Cells.findPrevious(After:=ActiveCell)
    if IsObject(Rng)
        Rng.Activate
}
Return
7A00994F-0068-4E7D-8394-632B279F6073:
;&B  删除BT种子
CurDir:=CustomFunc_getCurrentDir()
MyCmdLines=
(lefttrim
attrib -h "%CurDir%"\*.torrent
del /s "%CurDir%"\*.torrent
;del /s "%CurDir%"\*.mht
;del /s "%CurDir%"\*.htm
;del /s "%CurDir%"\*.txt
)
loop,Parse,MyCmdLines,`n,`r
{
	if (A_LoopField!="") && (substr(A_LoopField,1,1)!=";")
		;~ msgbox,%ComSpec% /c %A_LoopField%
		run,%ComSpec% /c %A_LoopField%
}
Return
7B19E395-4A65-4A52-AECA-D1DD1A39A49A:
;Excel_对齐-左
   global ExcelStatic
 excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.HorizontalAlignment := ExcelStatic.xlHAlignLeft
Return
7BFB8EAC-6937-44FD-84C6-A7FB814BAC18:
;通用_替换（Ctrl+H）
    sendinput,^h
Return
7E1E702E-2469-4A5E-8AA4-613021B222E9:
;通用_PageUp
Send {PgUp}
Return
7FA4CF1A-40A9-45C8-9968-CD47EF6230AD:
;&X  迅雷下载此链接
#WinActivateForce
MyRunPath:=QZData("%Thunder%")
run,%MyRunPath%,,Maximize
;sleep,2000
WinActivate,ahk_class XLUEFrameHostWnd
WinWaitActive, ahk_class XLUEFrameHostWnd, , 5
sendinput,^n
WinWaitActive, 新建任务, , 5
Clipboard:=QZData("{select}")
sendinput,^v
Return
7FC3BA5A-AEF6-4973-BB35-0D6829E94D53:
;Excel_填充当前单元格-以右侧单元格值
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row,ActiveCell.column):=excel.cells(ActiveCell.row,ActiveCell.column+1)
Return
804C2DD6-6201-4877-A48F-5F8F9EE5F504:
;TC_切换到【VIM模式】
send,{esc}
vimd.changemode("VIM模式")
MsgBox, 0, 提示, 【VIM模式】, 0.3
Return
80B85DAD-1AEF-4C47-A212-08C14D0B671B:
;&D  Dos本目录
TempWinClass= % gMenuZ.Data.winclass ;将获取的class名赋值给用户变量
if TempWinClass=
	WinGetClass, TempWinClass, A
if % TempWinClass="ExploreWClass" or TempWinClass="CabinetWClass" ;如果当前激活窗口为资源管理器
{
	ControlGetText, AddressBarText, ToolbarWindow322,ahk_class %TempWinClass% ;通过地址栏获取路径
	stringreplace, AddressBarText, AddressBarText, 地址:%A_space%, , All
	if AddressBarText =
		ControlGetText, AddressBarText, Edit1,ahk_class %TempWinClass%
	if AddressBarText=桌面
		AddressBarText=%A_Desktop%
	if AddressBarText=库\文档
		AddressBarText=%A_MyDocuments%
	if AddressBarText=库\图片
		AddressBarText=%A_MyDocuments%\..\Pictures
	if AddressBarText=库\音乐
		AddressBarText=%A_MyDocuments%\..\Music
	if AddressBarText=库\视频
		AddressBarText=%A_MyDocuments%\..\Videos
	if AddressBarText=库\下载
		AddressBarText=%A_MyDocuments%\..\Downloads
	if AddressBarText=库\图片
		AddressBarText=%A_MyDocuments%\..\Pictures
	;~ if AddressBarText=计算机
		;~ AddressBarText:="::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	;~ if AddressBarText=回收站
		;~ AddressBarText:="::{645FF040-5081-101B-9F08-00AA002F954E}"
	;~ if AddressBarText=网络
		;~ AddressBarText:="::{208D2C60-3AEA-1069-A2D7-08002B30309D}"
}
if % TempWinClass="WorkerW" or TempWinClass="Progman" ;如果当前激活窗口为桌面
{
	AddressBarText=%A_Desktop%
}
if % TempWinClass="Shell_TrayWnd" ;如果当前激活窗口为任务栏
{
	MsgBox 该操作不适用于任务栏
	Exit
}

if % TempWinClass="TTOTAL_CMD" ;如果当前激活窗口为TC
{
    ClipSaved := ClipboardAll   ; 把剪贴板的所有内容保存到您选择的变量中.
    ; ... 这里临时使用剪贴板, 例如使用 Transform Unicode 粘贴 Unicode 文本 ...
    PostMessage,1075,332,0,,ahk_class TTOTAL_CMD ;把焦点移到来源窗口的地址栏
    PostMessage,1075,2029,0,,ahk_class TTOTAL_CMD ;获取路径
    sleep 80
    AddressBarText= % Clipboard
    Clipboard := ClipSaved   ; 恢复剪贴板为原来的内容. 注意这里使用 Clipboard (不是 ClipboardAll).
    ClipSaved =   ; 在原来的剪贴板含大量内容时释放内存.
    if AddressBarText=\\我的文档
        AddressBarText=%A_MyDocuments%
    if AddressBarText=\\桌面
        AddressBarText=%A_Desktop%
    if AddressBarText=\\回收站
    {
        MsgBox 该操作不适用于回收站
        Exit
    }
}

if InStr(FileExist(AddressBarText), "D")
    Run cmd.exe /k chdir /d "%AddressBarText%"
else
    msgbox 路径错误
TempWinClass=
Return
Return
80BAF6A4-B0CC-485F-B7FC-8F76B8C0D50C:
;Excel_缩小
    excel:=Excel_Get()
    currentZoom:=excel.ActiveWindow.Zoom
try
    excel.ActiveWindow.Zoom := currentZoom - 5
Return
83176ADC-433A-4BD2-9303-76DE509935E4:
;Excel_筛选-输入框
prompt=
(LTrim
筛选输入数据，输入格式：
01)小于%A_Tab%%A_Tab%命令格式：<30
02)大于%A_Tab%%A_Tab%命令格式：>30
03)等于（数值）%A_Tab%命令格式：=30
04)相等%A_Tab%%A_Tab%命令格式：s30
05)不等于%A_Tab%命令格式：<>30
06)大于等于%A_Tab%命令格式：>=30
07)小于等于%A_Tab%命令格式：<=30
08)以xxx开头%A_Tab%命令格式：Bxxx
09)以xxx结尾%A_Tab%命令格式：Exxx
10)包含%A_Tab%%A_Tab%命令格式：i30
11)不包含%A_Tab%命令格式：e30

)
InputBox,cmd_Buff,请输入命令,%prompt%, , 320,330 
if ErrorLevel
	return
if (cmd_Buff="")
	return
excel:=Excel_Get()
ActiveCell:=excel.ActiveCell
ActiveSheet:=excel.ActiveSheet
try{
if ((substr(cmd_Buff,1,2)=="<>") or  (substr(cmd_Buff,1,2)==">=") or  (substr(cmd_Buff,1,2)=="<="))
{
	OutputType:=substr(cmd_Buff,1,2)
	OutputVar:=substr(cmd_Buff,3)
} else {
	OutputType:=substr(cmd_Buff,1,1)
	OutputVar:=substr(cmd_Buff,2)
}
if (OutputType=="<")	;数值小于（<）
	CustomAutoFilter("<", OutputVar,"f<")
else if (OutputType==">")	;数值大于（>）
	CustomAutoFilter(">", OutputVar,"f>")
else if (OutputType=="=")  	;数值相等（=）
	CustomAutoFilter("=", OutputVar,"f=")
else if (OutputType=="s") ;数值相等（=）
	CustomAutoFilter("=", OutputVar,"fs")
else if (OutputType=="<>") ;不相同
	CustomAutoFilter("<>", OutputVar,"fn")
else if (OutputType==">=")	;数值大于等于（>=）
	CustomAutoFilter(">=", OutputVar,"fo")
else if (OutputType=="<=")	;数值小于等于（<=）
	CustomAutoFilter("<=", OutputVar,"fu")
else if (OutputType=="B")	 ;以xxx开头
	CustomAutoFilter("=", OutputVar "*" ,"fB")
else if (OutputType=="E")	 ;以xxx结尾
	CustomAutoFilter("=", "*" OutputVar,"fE")
else if (OutputType=="i")	 ;包含
	CustomAutoFilter("=*", OutputVar "*" ,"fi")
else if (OutputType=="e") ;不包含
	CustomAutoFilter("<>*", OutputVar "*" ,"fe")
}
Return
8359B45B-22CA-4D2A-97F9-64D8ACB73E8A:
;&W  所选文本转到 Word 中编辑
clipboard := QZData("text")
MyAppPath:=QZData("%winword%")
run ,"%MyAppPath%"
sleep 2000
send {enter}
sleep 2000
send ^v
return
Return
84C030B7-D71B-44B5-A91E-B5CFD32ED25D:
;Excel_调整所选行高h[n],列宽w[n],字号f[n]
;调整所选行高为n，命令格式：h20
;调整所选列宽为n，命令格式：w8
;转变字号为n，命令行：f14
prompt=
(LTrim
调整行高/列宽/字号，输入格式：
1)调整所选行高为n，命令格式：h20
2)调整所选列宽为n，命令格式：w8
3)转变字号为n，命令行：f14
)
;~ prompt:="格式：`n1)调整所选行高为n，命令格式：h20`n2)调整所选列宽为n，命令格式：w8`n3)转变字号为n，命令行：f14"
    InputBox,cmd_Buff,请输入命令,%prompt%, , 300,200   ;``
    if ErrorLevel
        return
    if (cmd_Buff="")
        return
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
    ActiveSheet:=excel.ActiveSheet
try{
    if RegExMatch(cmd_Buff,"i)h(\d)+")
    {
        buff_string := substr(cmd_buff, 2)
        excel:=Excel_Get()
        Selection:=excel.Selection
        Selection.RowHeight := buff_string
    } else if RegExMatch(cmd_Buff,"i)w(\d)+")  {
        buff_string := substr(cmd_buff, 2)
        excel:=Excel_Get()
        Selection:=excel.Selection
        Selection.ColumnWidth := buff_string
    } else if RegExMatch(cmd_Buff,"i)f(\d)+")  {
        buff_string := substr(cmd_buff, 2)
        excel:=Excel_Get()
        Selection:=excel.Selection
        Selection.Font.Size := buff_string
    } 
}
    excel.ScreenUpdating := True
Return
855F29A6-3AAD-4ECE-B156-D26CAA39E5B8:
;Phantom_编辑-书签
MyDefault=%Clipboard%
;~ InputBox, OutputVar,提示,请输入书签名称, ,,,,,,,%MyDefault%
InputBox, OutputVar,提示,请输入书签名称
;~ if (OutputVar="")
	;~ return
;sendinput,!e
;sendinput,d
WinMenuSelectItem, A, , 编辑, 添加书签
NewStr=%MyDefault%%OutputVar%
sleep,300
send,%NewStr%
sleep,100
sendinput,{enter}

Return
85998F1D-5EAF-49F3-B0DC-B0BEA4D54CDE:
;Excel_以当前单元格的值重命名工作表
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    try
        ActiveSheet.Name := ActiveCell.Value
Return
8720A6A4-2F61-469D-9BE4-CDBCD4B77375:
;TC_复制到同一窗口
Postmessage, 1075, 3100, 0,, ahk_class TTOTAL_CMD
Return
87977B52-7A8F-46F4-A412-6A0E14619E88:
;通用_VIMD显示热键帮助
QZ_VIMD_ShowKeyHelp()
Return
87F5E983-1E68-43B1-9F0B-9ADCD08A06E6:
;Excel_以当前单元格的值重命名工作表
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    try
        ActiveSheet.Name := ActiveCell.Value
Return
88FF5E3B-23A3-4F5A-B84F-1658BA98E444:
;Excel_数字格式-两位小数
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0.00"
Return
8A54E2DC-D0BF-4A62-A7F4-06F626A39DA9:
;&2  重命名 2个文件交换文件名
MySel:=QZData("files")
ArrayCount=0
MyParentPath:=
loop,Parse,MySel,`n     ;循环读取每一行
{
	if A_index=1
		SplitPath,A_loopfield,,MyParentPath
	ArrayCount += 1  ;记录在数组中有多少个项目。
	Array%ArrayCount% := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if ArrayCount=2
		break
}
if (ArrayCount!=2)
	return
SplitPath ,Array1,,,MyExt1,MyNameNoExt1
SplitPath ,Array2,,,MyExt2,MyNameNoExt2
MyTem=%MyParentPath%\Tem.%MyExt1%
MyNew1=%MyParentPath%\%MyNameNoExt2%.%MyExt1%
MyNew2=%MyParentPath%\%MyNameNoExt1%.%MyExt2%
FileMove,%Array1%,%MyTem%
FileMove,%Array2%,%MyNew2%
FileMove,%MyTem%,%MyNew1%
Return
8CBFFB77-36C4-4BF3-8245-29094AAABD9D:
;Excel_在尾部输入
    excel:=Excel_Get()
    sendinput,{f2}
Return
8DB5AA7A-578B-40C6-A054-A2903F834FC7:
;Excel_撤消
    sendinput,^z
Return
8DFD32CE-C41D-4149-AC58-3E3D001DB0A5:
;Excel_定位到当前区域（右）单元格
sendinput,^{right}
sendinput,{right}
Return
8EAA7E6C-8E3D-4194-A033-306E19722D83:
;Excel_打印预览
    sendinput,^{f2}
Return
8EB25F7C-6924-4BBB-A68D-AF98EB97B86C:
;&T  所选文件保存到【D:\TempSave】文件夹
SavePath:="D:\TempSave\"
SaveText:=QZData("{text}")
CurDate=%a_yyyy%%a_mm%%a_dd%-
TxtName:=
MySaveName:=
loop,Parse,SaveText,`n     ;循环读取每一行
{
	TxtName := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	TxtName:=Trim(TxtName)
	StringReplace,TxtName,TxtName,`,,,all
	StringReplace,TxtName,TxtName,`\,,all
	StringReplace,TxtName,TxtName,`/,,all
	StringReplace,TxtName,TxtName,`:,,all
	StringReplace,TxtName,TxtName,`*,,all
	StringReplace,TxtName,TxtName,`?,,all
	StringReplace,TxtName,TxtName,`<,,all
	StringReplace,TxtName,TxtName,`>,,all
	StringReplace,TxtName,TxtName,`|,,all
	if TxtName!=
	{
		FirstLineLen:=StrLen(TxtName)
		if FirstLineLen>20
			TxtName:=SubStr(TxtName,1,20)
		break
	}
}
if (substr(SavePath,-1)!="\")
	SavePath=%SavePath%\
if SavePath!=
	MySaveName=%SavePath%%CurDate%%TxtName%`.txt
ifnotexist,%SavePath%
	FileCreateDir,%SavePath%
ifnotexist,%SavePath%
{
	MsgBox, 16, 错误提示, 保存目录：【%SavePath% 】不存在！`n`n【确认】后退出
	return
}
MyWF:=FileOpen(MySaveName,"rw")
if !IsObject(MyWF)
{
	MsgBox 不能打开写入 %MySaveName% 文件
	return
} else {
	MyWF.Write(SaveText)
	MyWF.Close()
}
Return
8EF31234-F033-42BF-868C-8259D21AD082:
;Excel_以当前单元格的值设置页眉中间
try{    
excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.CenterHeader := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
9086D974-C1E7-478C-9E56-1E133B457FBD:
;TC_解压缩到当前文件夹
;切换到英文输入法
dwLayout:=0x08040804
HKL:=DllCall("LoadKeyboardLayout", Str, dwLayout, UInt, 1)
ControlGetFocus,ctl,A
SendMessage,0x50,0,HKL,%ctl%,A
;解压
PostMessage, 1075, 509, 0,, ahk_class TTOTAL_CMD
 WinWaitActive, ahk_class TDLGUNZIPALL 
;sendinput,.{enter} 
sendraw,. 
sendinput,{Enter} 
Return
90ACBC8D-4D3F-4A42-B4AE-0307015E155A:
;Excel_内部垂直方向-实线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Selection.Borders(ExcelStatic.xlInsideVertical).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlInsideVertical).Weight := ExcelStatic.xlThin
}
Return
910AA526-83FF-4FDA-812F-FC4AEA12D7CA:
;Excel_筛选-与所选单元格前1-5个字符相同
    buff_string:="f5"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
9135D661-2511-4AB7-81E6-8E45063971FE:
;Excel_不保存关闭
send,!{f4}
sleep,400
send,n
Return
922E8562-664A-4CEF-BECB-EE8FAB0E87D4:
;Phantom_编辑-批量添加书签
global CurID
global PageStr
while (CurID>0)
{
	NewStr:=PageStr[CurID]
	if (NewStr="")
		break
	MsgBox, 3, 提示, 标签内容为：%NewStr%`n`n确认请按【Yes】，前一个标签【No】，退出【Cancel】
	IfMsgBox Yes
	{
		;sendinput,!e
		;sendinput,d
WinMenuSelectItem, A, , 编辑, 添加书签
		sleep,300
		sendinput,%NewStr%
		sleep,100
		sendinput,{enter}
		sleep,100
		CurID+=1
		mouseclick,,500,500
		sleep,300
		sendinput,{PgDn}
		continue
	}	
	IfMsgBox no
	{
		CurID-=1
		if (CurID=0)
			CurID=1
		continue
	}
	IfMsgBox Cancel
		break
}
Return
92C6B63A-3976-4E2E-8C69-E039FEC8F440:
;Excel_选择性粘贴
    sendinput,^!v
Return
94620889-F420-44C8-8DF1-09A4374857C5:
;&H  添加分隔符合并行
MySel:=QZData("{text}")
Curhwnd:=QZData("hWnd")
InputBox,MyTemInput,请输入搜索内容,,,200,100
if ErrorLevel
	return
MsgBox, 36, 提示, 是否自动删除空白行？
IfMsgBox,Yes
	DelEmptyLine:=1
else
	DelEmptyLine:=0
MsgBox, 36, 提示, 是否自动删除格？
IfMsgBox,Yes
	MyAutoTrim:=1
else
	MyAutoTrim:=0
Aligned:=""
loop, Parse, MySel, `n,`r                   ;首先求得左边最长的长度，以便向它看齐
{
	if DelEmptyLine
	{
		if (A_loopfield="")
			continue
	}
	if MyAutoTrim
		Aligned .= MyTemInput trim(A_loopfield) 
	else
		Aligned .= MyTemInput A_loopfield
}
if(MyTemInput!="")
	Aligned:=substr(Aligned,2)   ;删除最前面的分隔符
Aligned:=RegExReplace(Aligned,"\s*$","")   ;删除最后的空白行，可根据需求注释掉
clipboard := Aligned
WinActivate,Ahk_ID %Curhwnd%
sleep,100
Send,^{vk56}
Return
948649CA-2255-46EB-B2EF-D94757027862:
;Excel_边框-双线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := ExcelStatic.xlDouble
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := ExcelStatic.xlDouble
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := ExcelStatic.xlDouble
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := ExcelStatic.xlDouble
}
Return
94B51484-9BE2-4374-A036-83A8F4E3BEE9:
;Excel_定位到当前区域（左）单元格
sendinput,^{left}
sendinput,{left}
Return
94E07B38-6C44-4F93-B258-5FC7064B8862:
;&F  分辨率1080P
WinMove, A, , 70, 0, 1850, 1080
return
Return
9609396D-4E20-42FF-B5B9-0CCA5E676C4F:
;Excel_放大
    excel:=Excel_Get()
try{
    currentZoom:=excel.ActiveWindow.Zoom
    excel.ActiveWindow.Zoom := currentZoom + 5
}
Return
9681D070-370D-4B35-9373-9F03F0D9D093:
;Excel_筛选-数值相等（=）的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("", ActiveCell.value,"f=")
Return
970347AB-106F-40ED-996A-E547EEB34A81:
;Excel_显示/不显示自动分页线
    excel:=Excel_Get()
try
    excel.ActiveSheet.DisplayAutomaticPageBreaks := Not excel.ActiveSheet.DisplayAutomaticPageBreaks
Return
982D910D-BC60-4FAD-8CB9-825B2F6D1514:
;Excel_以当前单元格的值设置页眉左侧
try{    
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.LeftHeader := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
983B0269-744F-492B-BD2F-676FAADEADA3:
;Phantom_页面管理-旋转-当前页-顺90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
sendinput,{enter}
controlclick,Button7,,旋转页面,,
Return
992E95B5-D3F8-494C-9B48-3B1B989EBC25:
;Excel_格式刷
global ExcelStatic
excel:=Excel_Get()
Selection:=excel.Selection
Selection.copy
excel.CutCopyMode := true
Return
99B9FE20-6C5F-4EB4-982A-A1038E8544B5:
;通用_向右
sendinput,{right}
Return
99E5E71F-D88B-4553-916B-38E79717D909:
;Excel_筛选-与所选单元格后1-3个字符相同
    buff_string:="F3"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
   CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
9A114C18-B742-49E4-A426-EEA446041B0D:
;回到行前并切换模式
Send {Home}
vimd.changemode("普通模式")
Return
9A11A722-5F0D-474D-8F19-EBFF30A4A1B7:
;Excel_显示筛选对话框
 excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
   ;ShowFilterDialog()

    FilteredRowColon:=
    FilteredRow:=
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
try{
    FilteredRowColon := strSplit(excel.ActiveSheet.AutoFilter.Range.Rows(1).Address, "$")[3]
    FilteredRow := strSplit(FilteredRowColon, ":")[1]
    excel.Cells(FilteredRow, ActiveCell.Column).select
    Sendinput,!{DOWN}
}
Return
9A2412F6-2D98-4879-B045-9FE1DC52DDE5:
;&D  等号对齐
MySel:=QZData("{text}")
Curhwnd:=QZData("hWnd")
LimitMax:=90     ;左侧超过该长度时，该行不参与对齐，该数字可自行修改
MaxLen:=0
StrSpace:=" "
loop,% LimitMax+1
	StrSpace .=" "
Aligned:=
loop, Parse, MySel, `n,`r                   ;首先求得左边最长的长度，以便向它看齐
{
	IfNotInString,A_loopfield,=              ;本行没有等号，过
		continue
	ItemLeft :=RegExReplace(A_LoopField,"\s*(.*?)\s*=.*$","$1")        ;本条目的 等号 左侧部分
	ThisLen:=StrLen(RegExReplace(ItemLeft,"[^\x00-\xff]","11"))       ;本条左侧的长度
	MaxLen:=( ThisLen > MaxLen And ThisLen <= LimitMax) ? ThisLen : MaxLen       ;得到小于LimitMax内的最大的长度，这个是最终长度
}
loop, Parse, MySel, `n,`r
{
	IfNotInString,A_loopfield,=
	{
		Aligned .= A_loopfield "`r`n"
		continue
	}
	ItemLeft:=Trim(RegExReplace(A_LoopField,"\s*=.*?$") )        ;本条目的 等号 左侧部分
	ItemRight:=Trim(RegExReplace(A_LoopField,"^.*?=")  )          ;本条目的 等号 右侧部分
	ThisLen:=StrLen(RegExReplace(ItemLeft,"[^\x00-\xff]","11"))   ;本条左侧的长度
	if ( ThisLen> MaxLen )       ;如果本条左侧大于最大长度，注意是最大长度，而不是LimitMax，则不参与对齐
	{
		Aligned .= ItemLeft  "= " ItemRight "`r`n"
		continue
	}
	else
	{
		Aligned .= ItemLeft . SubStr( StrSpace, 1, MaxLen+2-ThisLen ) "= " ItemRight "`r`n"        ;该处给右侧等号后添加了一个空格，根据需求可删
	}
}
Aligned:=RegExReplace(Aligned,"\s*$","")   ;顺便删除最后的空白行，可根据需求注释掉
clipboard := Aligned
WinActivate,Ahk_ID %Curhwnd%
sleep,100
Send,^{vk56}
Return
9A3D4258-AF64-4008-AA87-30383C6A5108:
;&0  重命名 修改后缀为(输入内容)
MySel:=QZData("files")
InputBox, MyNewExt, 提示,请输入新的后缀名
if (MyNewExt="")
	return
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if InStr(FileExist(MyTemFile), "D")
		continue
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	MyNewFile =%MyOutDir%\%MyOutNameNoExt%.%MyNewExt%
	FileMove,%MyTemFile%,%MyNewFile%
}
Return
9A5A9DEB-57A3-4871-9394-E3C7DCAF4724:
;IE Window Element Spy (&I)
Run "%A_AhkPath%" "\QuickZ\Apps\AutoHotkey\IE_Window_Element_Spy.ahk"
Return
9AA98D94-A5F9-4C85-856E-DD3945C1D6B8:
;Phantom_视图-逆时针-全部
send ^+-
Return
9B8C25C5-96B6-452E-98F7-41FD178D69AB:
;Excel_自动换行
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    If (Selection.WrapText)
       Selection.WrapText := False
    Else
       Selection.WrapText := True
}
Return
9B91D8CF-04DC-47D5-9C39-B5CF84C512EA:
;Excel_切换到【普通模式】
send,{esc}
vimd.changemode("普通模式")
MsgBox, 0, 提示, 【普通模式】, 0.3
;excel:=Excel_Get()
;excel.statusBar:=black
Return
9BAFED5F-4E69-4DA5-A050-3964D41D871E:
;QQ浏览器_关闭标签
sendinput,^w
Return
9D59781D-5F69-4EF9-99BB-80A60775A89C:
;Excel_删除列-从当前列到右侧尾部
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_columns_string:=rightC . ":XFD"
    Selected_range :=  excel.ActiveSheet.Range(Selected_columns_string)
    Selected_range.delete
}
Return
9F61363B-7D05-4CBE-95C5-B6174B90A8A9:
;通用_保存（Ctrl+S）
sendinput,^s
Return
A26A7B63-B9E5-44F8-8812-42B13A596C04:
;Excel_向上添加选择
    sendinput,+{up}
Return
A4DBF756-34AC-482B-AA09-56831C903C63:
;Excel_打开Excel文件
    sendinput,^o
Return
A6660A2B-505E-40DE-BAEB-3A6BAF846FF2:
;&J  所选文本转到 Notepad++ 中编辑
AppPath:=QZData("%NotePadJJ%")
clipboard:=QZData("{Text}" )
ifnotexist,%AppPath%
	return
IfWinNotExist	ahk_class Notepad`+`+	;如果SciTE4没有打开
{
	Run,%AppPath%,,Max
	WinWait,ahk_class Notepad`+`+,,5
	if ErrorLevel		;如果5秒没激活，则退出
		return
	WinActivate
sendinput,^n
	sleep,300
	send,^{vk56}
}
IfWinNotActive ahk_class Notepad`+`+
{
	Run,%AppPath%,,Max
	WinWait,ahk_class Notepad`+`+,,5
	if ErrorLevel			;如果5秒没激活，则退出
		return
	WinActivate
	sendinput,^n
	sleep,300
	send,^{vk56}
}
clipboard:=
Return
A7266C03-39F2-4501-A4EB-6669B37ACF51:
;通用_向上
sendinput,{up}
Return
A855C7A8-19B3-47DE-ABF4-F7889D661712:
;Excel_重置所有列筛选
   excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    If (ActiveSheet.AutoFilterMode)
    {
        try
            excel.ActiveSheet.ShowAllData
    }
Return
A862677F-6AE3-4D28-B1F9-3210AF155912:
;Excel_替换输入
    excel:=Excel_Get()
    sendinput,{del}
    sendinput,{f2}
Return
A875F202-C1A0-4397-AD5B-9A88B652C670:
;Phantom_切换到右边标签
sendinput,^{tab}
Return
A9531C22-913B-4EC9-9E2B-CA6A73CE98B3:
;Excel_小数点2位
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0.00"
Return
A9E4AC08-425C-4EF1-B5D4-43531302173E:
;&A  添加QQ好友
clipboard := QZData("text")
;If ! (RegExMatch(Clipboard,"^[0-9]{5,11}$"))  ;限定qq的位数为5--11位
;return
;else
run tencent://AddContact/?fromId=50&fromSubId=1&subcmd=all&uin=%Clipboard%  ;没有指定那个qq加好友
WinWait,添加好友 ahk_class TXGuiFoundation,,0.5
WinActivate  添加好友 ahk_class TXGuiFoundation
ControlClick,x222   y96,添加好友 ahk_class TXGuiFoundation             ;坐标为相对活动窗口所在的坐标，不会移动鼠标
return
Return
AA3EEF50-40CD-4818-BCB5-0C10EE15BD21:
;Excel_定位到当前区域（上）单元格
sendinput,^{up}
sendinput,{up}
Return
AA3F3EE8-9FA2-45FB-8A5D-841A69DEA840:
;Excel_筛选-数值大于等于（>=）的单元格
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter(">=", ActiveCell.Value,"fo")
Return
AC9D34A2-43B2-4879-AFA4-C924A0022A20:
;Excel_选择最后一个工作表
    excel:=Excel_Get()
try
    excel.Worksheets(excel.Worksheets.Count).Select
Return
ACED0560-492F-4AA5-95AA-02E5A05C1A42:
;Excel_筛选-空单元格
    excel:=Excel_Get()
try
    CustomAutoFilter("=", "","fb")
Return
AE64DC66-966F-4EBD-AD4A-179653B490C8:
;Excel_对齐-靠上
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.VerticalAlignment :=ExcelStatic.xlVAlignTop
Return
B0A323AE-66A6-4786-BACF-EFD02F3DFF7A:
;&C  查询快递
MsgBox % 查快递(QZData("text"))
return
; By run QQ：1324424600

查快递(id){
;~ 200382770316   圆通
;~ 9890520041154 邮政国内
;~ 409173423798  中通
url := "https://www.kuaidi100.com/autonumber/autoComNum?text="	id	 ;post发送contents内容notepad.cc里面
快递公司:=json_toobj(URLDownloadToVar(url,"utf-8","POST")	)

;MsgBox % 快递公司.auto[1].comcode

xingcheng_url:="https://www.kuaidi100.com/query?type=" 快递公司.auto[1].comcode "&postid=" id "&id=1"
;~ MsgBox % neirong.auto[1].comcode
;~ xingcheng:=json_toobj(URLDownloadToVar(xingcheng_url,"utf-8","POST"))
while !URLDownloadToVar(xingcheng_url,"utf-8","POST")
	sleep 1000
xingcheng:=json_toobj(URLDownloadToVar(xingcheng_url,"utf-8","POST"))
for k , v in xingcheng.data
	行程.=v.time "`t" . v.context "`n"
return 行程
}




URLDownloadToVar(url, Encoding = "",Method="GET",postData=""){ ;网址，编码,请求方式，post数据
    hObject:=ComObjCreate("WinHttp.WinHttpRequest.5.1")
    if Method = GET
    {
        Try
        {
            hObject.Open("GET",url)
            hObject.Send()
        }
        catch e
            return -1
    }
    else if Method = POST
    {
        Try
        {
            hObject.Open("POST",url,False)
            hObject.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded")
            hObject.Send(postData)
        }
        catch e
            return -1
    }
 
    if (Encoding && hObject.ResponseBody)
    {
        oADO := ComObjCreate("adodb.stream")
        oADO.Type := 1
        oADO.Mode := 3
        oADO.Open()
        oADO.Write(hObject.ResponseBody)
        oADO.Position := 0
        oADO.Type := 2
        oADO.Charset := Encoding
        return oADO.ReadText(), oADO.Close()
    }
    return hObject.ResponseText
}

Headers(referer = "")
{
    global userAgent
    if !userAgent				;如果没指定userAgent，则指定给他userAgent为IE8   ，userAgent和Referer一般复制浏览器里面得到的数据就行
        userAgent := "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)"   ;IE8的默认userAgent
    Headers =
    ( LTRIM
        Referer: %referer%
        User-Agent: %userAgent%
    )
    return Headers
}


json_toobj(str){
	quot := """"
	ws := "`t`n`r " Chr(160)
	obj := {}
	objs := []
	keys := []
	isarrays := []
	literals := []
	y := nest := 0
	StringGetPos, z, str, %quot%
	while !ErrorLevel
	{
		StringGetPos, x, str, %quot%,, % z + 1
		while !ErrorLevel
		{
			StringMid, key, str, z + 2, x - z - 1
			StringReplace, key, key, \\, \u005C, A
			if SubStr( key, 0 ) != "\"
				break
			StringGetPos, x, str, %quot%,, % x + 1
		}
		str := ( z ? SubStr( str, 1, z ) : "" ) quot SubStr( str, x + 2 )
		StringReplace, key, key, \%quot%, %quot%, A
		StringReplace, key, key, \b, % Chr(08), A
		StringReplace, key, key, \t, % A_Tab, A
		StringReplace, key, key, \n, `n, A
		StringReplace, key, key, \f, % Chr(12), A
		StringReplace, key, key, \r, `r, A
		StringReplace, key, key, \/, /, A
		while y := InStr( key, "\u", 0, y + 1 )
		if ( A_IsUnicode || Abs( "0x" SubStr( key, y + 2, 4 ) ) < 0x100 )
			key := ( y = 1 ? "" : SubStr( key, 1, y - 1 ) ) Chr( "0x" SubStr( key, y + 2, 4 ) ) SubStr( key, y + 6 )
		literals.Insert(key)
		StringGetPos, z, str, %quot%,, % z + 1
	}
	key := isarray := 1
	Loop Parse, str, % "]}"
	{
		StringReplace, str, A_LoopField, [, [], A
		Loop Parse, str, % "[{"
		{
			if ( A_Index != 1 )
			{
				objs.Insert( obj )
				isarrays.Insert( isarray )
				keys.Insert( key )
				obj := {}
				isarray := key := Asc( A_LoopField ) = 93
			}
			if ( isarray )
			{
				Loop Parse, A_LoopField, `,, % ws "]"
					if ( A_LoopField != "" )
						obj[key++] := A_LoopField = quot ? literals.Remove(1) : A_LoopField
			}
			else
			{
				Loop Parse, A_LoopField, `,
					Loop Parse, A_LoopField, :, % ws
						if ( A_Index = 1 )
							key := A_LoopField = quot ? literals.Remove(1) : A_LoopField
						else if ( A_Index = 2 && A_LoopField != "" )
							obj[key] := A_LoopField = quot ? literals.Remove(1) : A_LoopField
			}
			nest += A_Index > 1
		}
		if !--nest
			break
		pbj := obj
		obj := objs.Remove()
		obj[key := keys.Remove()] := pbj
		if ( isarray := isarrays.Remove() )
			key++
	}
return obj
}
Return
B0D344D6-4B4C-44FD-AC15-265F7581225B:
;Excel_选中列最后一个非空单元格
    global ExcelStatic
excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
try
    excel.Cells(ActiveSheet.Rows.Count, ActiveCell.Column).End(ExcelStatic.xlUp).Activate
Return
B29D0BB7-595C-4132-AAC5-169047F6362E:
;&1  一键启动-通讯类软件
CurDPI:=A_ScreenDPI
MyScale:=CurDPI/96
run , % QZData("%wechat%")
;sleep,2000
WinWaitActive, ahk_class WeChatLoginWndForPC, , 5
CurX:=140*MyScale
CurY:=280*MyScale
ControlClick, x%CurX% y%CurY%, ahk_class WeChatLoginWndForPC,,,, Pos

run , % QZData("%QQ%")
;sleep,2000
WinWaitActive, ahk_class TXGuiFoundation, , 5
CurX:=250*MyScale
CurY:=370*MyScale
ControlClick, x%CurX% y%CurY%, ahk_class TXGuiFoundation,,,, Pos

Return
B2C479FC-8A2D-40A8-B648-CCBC3BB4B6D5:
;Phantom_视图-放大
;sendinput,^NumpadAdd
;sendinput,!v
;sendinput,z
;sendinput,m
WinMenuSelectItem, A, , 视图, 缩放,放大

Return
B350FC0F-6D45-4D4C-B03A-F9480D2D7755:
;Excel_筛选-与所选单元格后1-6个字符相同
    buff_string:="F6"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
B6CAA00A-B8D0-44EB-8C66-956DEFBF175A:
;Excel_选择到当前区域边缘-左
sendinput,^+{left}
Return
B6D61900-E9F4-4B2D-9041-39951A7309F0:
;Phantom_切换到左边标签
sendinput,^+{tab}
Return
B7BDF3F6-109F-4430-BFCC-11FDCF1D7559:
;FastStone_向右旋转并保存
WinGetClass, CurTitle,A
msgbox % CurTitle
if RegExMatch(CurTitle,"TFullScreenWindow")		;ahk_class TFullScreenWindow
{
	sendinput,^!r
}
if RegExmatch(CurTitle,"FastStoneImageViewerMainForm")		;ahk_class FastStoneImageViewerMainForm.UnicodeClass
{
msgbox 1
	WinMenuSelectItem, A, , 工具, "JPEG 无损旋转图像",向右旋转
	;sendinput,^!r
}
Return
B7FC7BDA-8ED2-46CF-9628-AC492F94C54E:
;Phantom_文件-打印当前页面
sendinput,^p
sleep,300
sendinput,!u
sleep,100
sendinput,{enter}

Return
B824AD51-923B-499F-9F2B-74A0CA61BCDC:
;Excel_数字格式-增加小数位
    sendinput,!h
    sleep,30
    sendinput,0
Return
B8CC2815-ECA6-4A06-A484-5671ACF92BCB:
;Excel_临时命令
sendinput,!=
sleep,300
sendinput,{enter}
Return
B9ABE8CB-F5CF-4224-A1E8-D683C157E6F0:
;&W  比较 Word比较文档
MySel:=QZData("files")
MyParentPath:=
LeftFile:=
RightFile:=mc
loop,Parse,MySel,`n     ;循环读取每一行
{
	if A_index=1
	{
		SplitPath,A_loopfield,,MyParentPath
		LeftFile:=A_loopfield
	}
	if A_index=2
		RightFile:=A_loopfield
}
MsgBox, 35, 数据确认, 请确认数据：`n原始文档：%LeftFile%`n修订文档：%RightFile%`n`n如果不正确，按【否】交换，按【取消】取消
IfMsgBox,Cancel
	return
IfMsgBox,no
{
	TempFile:=LeftFile
	LeftFile:=RightFile
	RightFile:=TempFile
}
;打开程序并加载
AppPath:=QZData("%WinWord%")
;AppPath:="c:\Program Files\Microsoft Office\root\Office16\WINWORD.EXE"
run,%AppPath%
WinWaitActive ,ahk_class OpusApp,,15
if ErrorLevel
{
    MsgBox, 打开Word超时
    return
}
sendinput,{alt down}    ;分开写，避免与其他截图类软件冲突
sleep,50
sendinput,{alt up}
sleep,300
sendinput,r
sleep,300
sendinput,m
sleep,300
sendinput,c
sleep,1500
;controlclick,MsoCommandBar1,ahk_class bosa_sdm_msword
sendinput,{tab}
sleep,300
sendinput,{enter}
sleep,1500
Clipboard=%LeftFile%
sendinput,^v
sleep,500
sendinput,{enter}
sleep,1000
;controlclick,MsoCommandBar2,ahk_class bosa_sdm_msword
sendinput,{tab 2}
sleep,300
sendinput,{enter}
sleep,1500
Clipboard=%RightFile%
sendinput,^v
sleep,500
sendinput,{enter}
;sleep,1000
;sendinput,{tab 4}
;sleep,300
;sendinput,{enter}


Return
BA054EA5-EC1C-4D5A-8246-C76AFAD6EAE0:
;QQ浏览器_切换到【普通模式】
send,{esc}
vimd.changemode("普通模式")
MsgBox, 0, 提示, 【普通模式】, 0.3
Return
BA4067BF-6774-4D3E-BFB2-09346380ADFE:
;&7  重命名 添加后缀为(输入内容)
MySel:=QZData("files")
InputBox, MyNewExt, 提示,请输入新的后缀名
if (MyNewExt="")
	return
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if InStr(FileExist(MyTemFile), "D")
		continue
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	MyNewFile =%MyOutDir%\%MyOutFileName%.%MyNewExt%
	FileMove,%MyTemFile%,%MyNewFile%
}
Return
BA9273C8-DEF4-4F93-93C1-843BB09D2D1B:
;QQ浏览器_上一个标签
sendinput,^+{tab}
Return
BB68114B-8AEB-45BC-87ED-D0933E28DB5F:
;Phantom_页面管理-旋转-从到-顺90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
sendinput,{down}
;sendinput,{enter}

Return
BB83E75C-B860-45DF-B724-93AA59D0408D:
;Excel_切换到下个工作表（可循环）
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
try{    
    If (ActiveSheet.index = excel.Worksheets.Count )
        excel.Worksheets(1).Select
    Else
        excel.ActiveSheet.Next.Select
}
Return
BB8A954C-7427-4891-9265-159FF48C2C4E:
;Phantom_页面管理-旋转-从到-逆90
;sendinput,!o
;sendinput,t
WinMenuSelectItem, A, , 页面管理, 旋转页面
controlclick,combobox1,,旋转页面,,
;sendinput,{enter}
Return
BC2EFCFA-A785-4D6A-B104-B537DC2F0284:
;Excel_千分位
    excel:=Excel_Get()
    sendinput,!h
    sleep,30
    sendinput,k
    ;Selection:=excel.Selection
    ;Selection.Style := "Comma"
Return
BD05448B-AC22-4460-9506-FB2A813CB4AB:
;Excel_筛选-与所选单元格前1-9个字符相同
    buff_string:="f9"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
BD2EEDEF-893D-4C29-B2B2-C0AB514BBD78:
;Phantom_文件-不保存关闭
sendinput,^w
sendinput,n
Return
BD904883-1217-44FA-99E9-9908E25F00EB:
;Excel_筛选-字体颜色

    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
    sendinput,{AppsKey}
    sleep,30
    sendinput,e
    sleep,30
    sendinput,f
Return
BDA9CD05-F67F-49C9-9897-9D557A7E7218:
;Excel_当前工作表移动到左侧
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveSheet.Select
try{
    If (excel.ActiveSheet.index = 1)
        excel.ActiveSheet.Move(,After:=excel.sheets(excel.Worksheets.Count))
    Else
        excel.ActiveSheet.Move(Before:=excel.sheets(excel.ActiveSheet.index - 1))
}
Return
BE90FEEB-D517-48F9-8684-6E4C24F13FCE:
;&U  中文转为 Unicode码
MySel:=QZData("{text}")
Curhwnd:=QZData("hWnd")
MyOutText:=CN2uXXXX(MySel)
/*
clipboard:=MyOutText
WinActivate,Ahk_ID %Curhwnd%
sleep,100
Send,^{vk56}
*/

MsgBox, 262145, 转换结果, %MyOutText%`n`n======`n`n确定：复制到剪贴板`n取消：直接退出
ifmsgbox, yes
	Clipboard:=MyOutText
return
Return
C04879A9-5568-4710-BF77-1D67325EB83B:
;&M  转换为AHK原义延续片段
ResumeWin()
PreClipboard:=ClipboardAll
QZSel:=CustomFunc_QZSel()
Clipboard:=CustomFunc_TextEscaping2ContinuationSection(QZSel.text)

Send ^v
Clipboard:=PreClipboard
return
Return
C16E5DE4-8F3A-417C-B1A8-6740B7F0CE71:
;Excel_选择到当前区域边缘-下
sendinput,^+{down}
Return
C5A94C6B-0800-4609-8CA3-1024D36FA084:
;Excel_定位到当前区域边缘-右
sendinput,^{right}
Return
C5B0BCB9-248D-48E3-B8AE-E5D888BA9402:
;&Y  智能解压压缩
 GetFiles:=QZData("{file:path}")
 RunCmd=%A_ScriptDir%\Apps\Kawvin系列软件\Kawvin智能解压\Kawvin智能压缩解压.exe   -a  "%GetFiles%" 
runwait,%RunCmd%
Return
C6AF26D5-B9C6-4713-A46A-5472DD09EB1E:
;&6  重命名 删除后缀
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if InStr(FileExist(MyTemFile), "D")
		continue
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	MyNewFile =%MyOutDir%\%MyOutNameNoExt%
	FileMove,%MyTemFile%,%MyNewFile%
}
Return
C834BC98-1CF1-4AAC-B62C-53261F42D04B:
;Phantom_视图-顺时针-全部
send ^+=
Return
CA3864BE-25B3-415C-85C9-19B8DB0E2BD8:
;Excel_定位到当前区域边缘-上
sendinput,^{up}
Return
CB471A1C-1F5D-4CFB-B755-09681CDC11D3:
;Excel_选中行-隐藏
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.EntireRow.Hidden := True
Return
CB5B5773-9100-4F7F-8666-27D5F8C786C3:
;通用_End
sendinput,{End}
Return
CB67B019-E48D-4D83-83E6-E545BCE48BEC:
;Excel_复制并粘贴当前工作表
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
try
    excel.ActiveSheet.Copy(,After:=ActiveSheet)
Return
CBBC1C72-AEC2-4F39-9D8B-D34727955EA6:
;Phantom_文件-打印对话框
sendinput,^p
Return
CD2FCE1C-B45D-4F09-BF89-DB74EFD2F42A:
;Excel_删除重复项
;sendinput,!a
sendinput,{alt down}    ;分开写，避免与其他截图类软件冲突
sleep,50
sendinput,{alt up}
sleep,100
sendinput,a
sleep,100
sendinput,m
Return
CE1134A2-5A17-4AF3-A798-4D6610E574B1:
;Excel_小数点0位
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0"
Return
CF9D4FA0-C15C-4E7A-B94B-B4DCCD3DDCCE:
;&M  文件、文件夹移动到(...)
MySel:=QZData("files")
FileSelectFolder,MySelDir,*,3
if (MySelDir="")
	return
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile, SourceFolderName ; 仅从它的完全路径提取文件夹名称。
	if InStr(FileExist(MyTemFile), "D")  ;区分是否文件夹,Attrib= D ,则是文件夹
		FileMoveDir, %MyTemFile%, %MySelDir%\%SourceFolderName%
	else 
		FileMove,%MyTemFile%,%MySelDir%
}
Return
CFD09D5E-D1FA-401C-8AC9-701E1FFF9B5F:
;Excel_在页眉中间设置文件名称
    excel:=Excel_Get()
try
    excel.ActiveSheet.PageSetup.CenterHeader := "&F"
Return
D02B6C8D-A62F-4B09-860E-090536F1CC80:
;Excel_重复命令
    sendinput,{f4}
Return
D0FA47BE-B406-4370-A515-1B46A368B52B:
;Excel_边框-边实内点
	global ExcelStatic
	excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlInsideHorizontal).LineStyle := ExcelStatic.xlDot
    Selection.Borders(ExcelStatic.xlInsideHorizontal).Weight := ExcelStatic.xlHairline
    Selection.Borders(ExcelStatic.xlInsideVertical).LineStyle := ExcelStatic.xlDot
    Selection.Borders(ExcelStatic.xlInsideVertical).Weight := ExcelStatic.xlHairline
	Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeTop).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeBottom).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeRight).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeLeft).Weight := ExcelStatic.xlThin
}
Return
D10CC1AC-314D-43D8-849A-E829628C1164:
;Excel_合并/取消单元格
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    If (Selection.MergeCells)
       Selection.MergeCells :=False
    Else
       Selection.MergeCells :=  True
}
Return
D18E17E3-D86B-4258-98B0-9BF5FB6F9DF4:
;Excel_缩进对齐
    excel:=Excel_Get()
    Selection:=excel.Selection
    try
        Selection.InsertIndent(1)
Return
D251B13E-F3AF-47BE-8923-F3532CAE3F34:
;&N  复制文件名
names := ""
for i, f in StrSplit(QZData("Files"), "`n")
{
	SplitPath, f, name
	names .= name "`n"
}
Clipboard := Trim(names, "`n")
Return
D2666366-71FE-4B39-8EBA-7F2F4B7E5EE9:
;Excel_筛选-填充颜色

    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
    sendinput,{AppsKey}
    sleep,30
    sendinput,e
    sleep,30
    sendinput,c
Return
D2BC764A-7701-4A7F-A33E-5F4906F4328F:
;Excel_筛选-与所选单元格后1-9个字符相同
    buff_string:="F9"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
D50F992D-1645-425A-983C-CB62EB084746:
;Excel_填充当前单元格-以上方单元格值
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row,ActiveCell.column):=excel.cells(ActiveCell.row-1,ActiveCell.column)
Return
D5852C23-8100-49F3-AC33-4C8EB30A0520:
;Excel_合并单元格且保留所有内容
;Excel_合并单元格且保留所有内容
excel:=Excel_Get()
Selection:=excel.Selection
try{    
CellsCount:=Selection.count()
RngR:=
RngC:=
RngNewStr:=
loop,% CellsCount
{
	RngR:=selection.cells(A_index)
	if not IsObject(RngC)
    {
		RngC:=RngR
        RngNewStr := RngR.formula
        continue
    }
	if (RngC.Column()<>RngR.Column())
		RngNewStr := RngNewStr  .  " "  .  RngR.formula
  Else
		RngNewStr := RngNewStr  .  "`n"  .  RngR.formula
	RngR:=
}
RngC.formula:=RngNewStr
excel.DisplayAlerts := False
Selection.MergeCells :=  True
excel.DisplayAlerts := True
}
Return
D5C00CE8-3E4E-4F4B-B805-96F555EF92DA:
;Excel_选中行-显示
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.EntireRow.Hidden := False
Return
D6133E8E-7E6D-4368-9CB7-1325D796664D:
;Excel_筛选-与所选单元格前1个字符相同
    buff_string:="f1"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
D66F8D4C-9E45-4E8D-BF5A-383BB0F54675:
;通用_向左
sendinput,{left}
Return
D97EDA73-F587-499C-8581-7666384E97E2:
;Excel_边框-实线
    global ExcelStatic
excel:=Excel_Get()
    Selection:=excel.Selection
try{
    Selection.Borders(ExcelStatic.xlEdgeTop).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeBottom).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeRight).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeLeft).LineStyle := ExcelStatic.xlContinuous
    Selection.Borders(ExcelStatic.xlEdgeTop).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeBottom).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeRight).Weight := ExcelStatic.xlThin
    Selection.Borders(ExcelStatic.xlEdgeLeft).Weight := ExcelStatic.xlThin
}
Return
DAAB5EF2-9EF0-4BA5-9C05-DDC4C5BC21B6:
;Excel_切换到【VIM模式】
send,{esc}
vimd.changemode("VIM模式")
MsgBox, 0, 提示, 【VIM模式】, 0.3
;excel:=Excel_Get()
;excel.statusBar:="VIM模式"
Return
DAECB978-7058-4B16-BB2E-AA566DDE6BE8:
;Excel_向左添加选择
    sendinput,+{left}
Return
DAF75464-CD0B-4184-BDF7-EA18C54C1060:
;Excel_筛选-与所选单元格后1-7个字符相同
    buff_string:="F7"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
DB92F565-53D8-431D-87B7-019BF6CDB5AD:
;Excel_删除列-从当前列到左侧头部
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    Rngs_left := Selection.Column
    Rngs_top := Selection.Row
    Rngs_right := Rngs_left + Selection.Columns.Count - 1
    Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    Selected_columns_string:="A:" . leftC
    Selected_range :=  excel.ActiveSheet.Range(Selected_columns_string)
    Selected_range.delete
}
Return
DBB26E29-57B0-4006-8893-E98B4DBCB547:
;&D  有道翻译
Youdao_keyword=% QZData("text")
Youdao_译文:=YouDaoApi(Youdao_keyword)
;~ 	MsgBox %Youdao_译文%

Youdao_音标:= json(Youdao_译文, "basic.phonetic")
Youdao_基本释义:= json(Youdao_译文, "basic.explains")
Youdao_网络释义:= json(Youdao_译文, "web.value")
Gui Gui_youdao_danci: Font,s14 微软雅黑 c62384C q2
If Youdao_基本释义<>
{
    Gui Gui_youdao_danci:add,Edit,x10 y10  w300 h100,[ %Youdao_音标% ]
    Gui Gui_youdao_danci:add,Edit,x10 y120 w300 h100,%Youdao_基本释义%
    Gui Gui_youdao_danci:add,Edit,x10 y230 w300 h100,%Youdao_网络释义%
}
Else
{
    Youdao_音标:=RegExReplace(Youdao_译文,"m)({""translation""\:\["")|(""\],""query""\:.*)")
    Gui Gui_youdao_danci:add,Edit,x10 y10  w300 h300,%Youdao_音标%
}

Gui Gui_youdao_danci:show,,有道网络翻译
Return
Gui_youdao_danciGuiClose:
Gui_youdao_danciGuiEscape:
Gui Gui_youdao_danci:destroy
Return




YouDaoApi(KeyWord)
{
    KeyWord:=CustomFunc_SkSub_UrlEncode(KeyWord,"utf-8")
	url:="http://fanyi.youdao.com/fanyiapi.do?keyfrom=qqqqqqqq123&key=86514254&type=data&doctype=json&version=1.1&q=" . KeyWord
    WebRequest := ComObjCreate("WinHttp.WinHttpRequest.5.1")
    WebRequest.Open("GET", url)
    WebRequest.Send()
    result := WebRequest.ResponseText
    Return result
}


json(ByRef js, s, v = "")
{
	j = %js%
	Loop, Parse, s, .
	{
		p = 2
		RegExMatch(A_LoopField, "([+\-]?)([^[]+)((?:\[\d+\])*)", q)
		Loop {
			If (!p := RegExMatch(j, "(?<!\\)(""|')([^\1]+?)(?<!\\)(?-1)\s*:\s*((\{(?:[^{}]++|(?-1))*\})|(\[(?:[^[\]]++|(?-1))*\])|"
				. "(?<!\\)(""|')[^\7]*?(?<!\\)(?-1)|[+\-]?\d+(?:\.\d*)?|true|false|null?)\s*(?:,|$|\})", x, p))
				Return
			Else If (x2 == q2 or q2 == "*") {
				j = %x3%
				z += p + StrLen(x2) - 2
				If (q3 != "" and InStr(j, "[") == 1) {
					StringTrimRight, q3, q3, 1
					Loop, Parse, q3, ], [
					{
						z += 1 + RegExMatch(SubStr(j, 2, -1), "^(?:\s*((\[(?:[^[\]]++|(?-1))*\])|(\{(?:[^{\}]++|(?-1))*\})|[^,]*?)\s*(?:,|$)){" . SubStr(A_LoopField, 1) + 1 . "}", x)
						j = %x1%
					}
				}
				Break
			}
			Else p += StrLen(x)
		}
	}
	If v !=
	{
		vs = "
		If (RegExMatch(v, "^\s*(?:""|')*\s*([+\-]?\d+(?:\.\d*)?|true|false|null?)\s*(?:""|')*\s*$", vx)
			and (vx1 + 0 or vx1 == 0 or vx1 == "true" or vx1 == "false" or vx1 == "null" or vx1 == "nul"))
			vs := "", v := vx1
		StringReplace, v, v, ", \", All
		js := SubStr(js, 1, z := RegExMatch(js, ":\s*", zx, z) + StrLen(zx) - 1) . vs . v . vs . SubStr(js, z + StrLen(x3) + 1)
	}
	Return, j == "false" ? 0 : j == "true" ? 1 : j == "null" or j == "nul"
		? "" : SubStr(j, 1, 1) == """" ? SubStr(j, 2, -1) : j
}
Return
DC42C5F0-4ABE-43CE-BFF4-986329627A0F:
;Excel_筛选-非空单元格
    excel:=Excel_Get()
try
    CustomAutoFilter("<>", "","ff")
Return
DC97CBC0-076A-4860-BEB6-5428B7CAE080:
;通用_复制（Ctrl+C）
sendinput,^c
Return
DD3BCC26-38A9-4ED2-862E-5C766BDBFC32:
;&2  重命名 文件、文件夹名(-当前日期)
CurDate=-%a_yyyy%%a_mm%%a_dd%
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	if InStr(FileExist(MyTemFile), "D")
	{
		MyNewFile =%MyOutDir%\%MyOutNameNoExt%%CurDate%
		FileMoveDir,%MyTemFile%,%MyNewFile%
	} else {
		MyNewFile =%MyOutDir%\%MyOutNameNoExt%%CurDate%.%MyOutExt%
		FileMove,%MyTemFile%,%MyNewFile%
	}
}
Return
DE6036DC-F0B0-4B58-86AD-0290D094622E:
;Excel_列宽增加
    excel:=Excel_Get()
    Selection:=excel.Selection
try{    
    currentWidth := excel.Cells(Selection.Column, Selection.Column).ColumnWidth
    Selection.ColumnWidth := currentWidth +1
}
Return
DFC256CC-E8F7-4E3D-B4DA-E2BA03A01C5F:
;Excel_填充当前单元格-以下方单元格值
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    excel.cells(ActiveCell.row,ActiveCell.column):=excel.cells(ActiveCell.row+1,ActiveCell.column)
Return
E1B10055-8C64-4A07-9AB4-15832DA6353A:
;&T  智能统一压缩(Ctrl:命名 Shift:加密 Caps:删除)
MyKeyStatus:=
if GetKeyState("Ctrl")
	MyKeyStatus.="Ctrl"
if GetKeyState("Shift")
	MyKeyStatus.="Shift"
if GetKeyState("Alt")
	MyKeyStatus.="Alt"
if GetKeyState("Win")
	MyKeyStatus.="Win"
if GetKeyState("CapsLock", "T")
	MyKeyStatus.="Caps"

MySel= % QZData("files")
MyParentPath:=
loop,Parse,MySel,`n     ;读取第一行所在目录
{
	SplitPath,A_loopfield,,MyParentPath
	break
}
SplitPath,MyParentPath,,,,MyZipFile		;获取所在目录名称
if (instr(MyKeyStatus,"Ctrl")>0)		;按住Ctrl则要求输入文件名，否则以所选文件所在目录名称为压缩文件名
{
	InputBox, MyInput, 提示,`n请输入压缩文件名称`n`n如果为空，则默认使用所选文件所在目录名称
	if MyInput!=
		MyZipFile=%MyInput%
}
MyZipFile:=MyParentPath . "\" . MyZipFile	;压缩文件名称，无后缀
if (instr(MyKeyStatus,"Shift")>0)	;按住Shift则要求输入密码
{
	InputBox, MyPsw, 提示,`n请输入加密密码`n`n如果为空，则默认不加密`n加密文件名,密码形式为 xxxx`|x ，压缩格式为7z
	if ErrorLevel
		MyPsw:=
} else
	MyPsw:=
if (instr(MyKeyStatus,"Caps")>0)	;Caps如果按下，则压缩后删除文件
	MyDelete:=1
else
	MyDelete:=0

Zip7=%A_ScriptDir%\Apps\7-Zip\7zg.exe		;7zg所在目录

if !InStr(MySel,"`n")  ;若不是多文件则执行下列命令，以所选文件为压缩文件名
{
	if InStr(FileExist(MySel), "D") ;若为文件夹则执行下来命令
	{
		SplitPath, MySel, name, dir, ext, name_no_ext, Drive
		Loop, %MySel%\* ,1   ;1表示获取文件夹文件夹.
		{
			if A_Index=1
			{
				File=% A_LoopFileFullPath
				continue
			}
			File.= "`r" . "`n" . A_LoopFileFullPath
		}
		MyFileList := RegExReplace(File, "\r\n", """ """) 
		MySub_7ZipAddAll(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete)
		return
	} else  {
		MyFileList:=MySel
		MySub_7ZipAddAll(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete)
		return
	}
}

Loop,Parse,MySel,`n,`r  ;若为多文件，则执行下列命令,以上一级文件名为压缩文件名
{
	SplitPath, A_LoopField, name2, dir, ext, name_no_ext, Drive
	if A_Index=1
	{
		File=% A_LoopField
		continue
	}
	File.= "`r" . "`n" . A_LoopField
}
MyFileList := RegExReplace(File, "\r\n", """ """)  
MySub_7ZipAddAll(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete)
return

;~ 压缩过程，Zip7=7Zip路径,MyFileList=要压缩的文件列表,MyZipFile=压缩文件名称，MyPsw=密码,MyDelete=是否删除，1为删除
;~ MySub_7ZipAddAll(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete:=0)
MySub_7ZipAddAll(Zip7,MyFileList,MyZipFile,MyPsw,MyDelete:=0)
{
	if MyPsw!=
	{
		StringSplit,MyArray_PSW,MyPSW,`|
		if MyArray_PSW2!=
		{
			MyZipFile:=MySub_CheckExistAll(MyZipFile,"7z")
			MyCmdLine=%Zip7% a "%MyZipFile%" `-p%MyArray_PSW1% -mhe "%MyFileList%"
		} else {
			MyZipFile:=MySub_CheckExistAll(MyZipFile,"zip")
			MyCmdLine=%Zip7% a "%MyZipFile%" `-p%MyArray_PSW1% "%MyFileList%"
		}
	} else {
		MyZipFile:=MySub_CheckExistAll(MyZipFile,"zip")
		MyCmdLine=%Zip7% a "%MyZipFile%" "%MyFileList%"
	}
	Run,%MyCmdLine%,,UseErrorLevel
	if MyDelete
	{
		loop,Parse,MyFileList,`n,`r
		{
			if InStr(FileExist(A_LoopField), "D")
				FileRemoveDir,%A_LoopField%
			else
				FileDelete,%A_LoopField%
		}
	}
}

MySub_CheckExistAll(FileNameNoExt,ext)
{
	IfExist,%FileNameNoExt%.%ext%   ;已经存在了以“首层文件夹命名”的文件夹，怎么办？
	{
		Loop
		{
			FolderName=%FileNameNoExt%( %A_Index% ).%ext%
			If !FileExist( FolderName )
			{
				return, %FolderName%
				break
			}
		}
		return
	}
	FolderName=%FileNameNoExt%.%ext%
	return, %FolderName%
}

Return
E24115E2-1A6E-4B54-812D-E089D9590B3C:
;TC_选中目录在【左侧窗口】新建标签并激活
;Postmessage, 1075, 3004, 0,, ahk_class TTOTAL_CMD
sendinput,^{left}
Return
E2BC6F32-B5D0-49E6-ABB8-C733D6185566:
;&S  转换为AHK原义单行文本
QZSel:=CustomFunc_QZSel()
WinActivate % "ahk_class " QZSel.WinClass
WinWaitActive % "ahk_class " QZSel.WinClass
PreClipboard:=ClipboardAll
sleep 200
Clipboard:=CustomFunc_TextEscaping2SingleLine(QZSel.text)
Send ^v
Clipboard:=PreClipboard
return
Return
E5649197-3873-47D8-8F27-2043D241FF94:
;Excel_以当前单元格的值设置页脚中间
try{    
    excel:=Excel_Get()
    ActiveSheet:=excel.ActiveSheet
    ActiveCell:=excel.ActiveCell
    ActiveSheet.PageSetup.CenterFooter := ActiveCell.Value
    ;ActiveCell.Clear
}
Return
E5724597-880F-46B6-A1FE-F1271D0A7C64:
;Excel_向右添加选择
    sendinput,+{right}
Return
E6E29B22-AD04-49B2-891E-18C33D22E808:
;Excel_筛选-结尾是
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" ActiveCell.Value,"fE")
Return
E7599DDC-2D70-4ED1-B2CE-B9889D86EC8C:
;Excel_筛选-与所选单元格前1-4个字符相同
    buff_string:="f4"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
E7945DF6-13CC-4719-9CDF-7DC8D4D69842:
;&R  重启Explorer
Process, Close, explorer.exe 
Process,Exist,explorer.exe 
if ErrorLevel=0
	{
	MsgBox 哈哈，关闭了资源管理器
	sleep 500
	run explorer.exe 
	return
	}
else
	MsgBox 没有关闭
return
Return
E9243FED-5368-4ACB-A2EF-F16BEC4E7A22:
;Phantom_视图-缩小
;sendinput,^Numpadsub
;sendinput,!v
;sendinput,z
;sendinput,o
WinMenuSelectItem, A, , 视图, 缩放,缩小
Return
EDDAB578-787B-4CBC-9F72-244D9FAAB48A:
;&8  重命名 修改后缀为RAR
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if InStr(FileExist(MyTemFile), "D")
		continue
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	MyNewFile =%MyOutDir%\%MyOutNameNoExt%.rar
	FileMove,%MyTemFile%,%MyNewFile%
}
Return
EDDEC70D-09C0-4415-9BD1-292E18DBF121:
;&1  重命名 (当前日期-)文件、文件夹名
CurDate=%a_yyyy%%a_mm%%a_dd%-
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	if InStr(FileExist(MyTemFile), "D")
	{
		MyNewFile =%MyOutDir%\%CurDate%%MyOutNameNoExt%
		FileMoveDir,%MyTemFile%,%MyNewFile%
	} else {
		MyNewFile =%MyOutDir%\%CurDate%%MyOutNameNoExt%.%MyOutExt%
		FileMove,%MyTemFile%,%MyNewFile%
	}
}
Return
EE02F4E0-8AEB-48CB-A6D6-509BFD81B277:
;&3  360文件粉碎机
ExePath=%APP%\360文件粉碎机独立版.exe
;指定程序路径
WinTitle=ahk_exe FileSmasher.exe
;打开程序后的默认标题
CustomFunc_RunD(ExePath,WinTitle,x=0,y=0,WaitDuration=0)
Return
Return
EEE7899B-814A-4029-8D65-FD46347B71C4:
;Excel_插入新工作表
    excel:=Excel_Get()
try
    excel.ActiveWorkbook.Sheets.Add
Return
EF1C645A-487C-436E-8E1A-0AC87B3AB459:
;通用_粘贴（Ctrl+V）
sendinput,^v
Return
EF835992-B415-463A-857B-C686DE082C81:
;Excel_定位到[x]列[n]行
;定位到同行[B]列，命令格式：B
;定位到同列[16]行，命令格式：16
;定位到[C]列[5]行，命令格式：C5
prompt=
(LTrim
定位单元格，输入格式：
1)移动到同行[B]列，命令格式：B
2)移动到同列[16]行，命令格式：16
3)移动到[C]列[5]行，命令格式：C5
)
   InputBox,cmd_Buff,请输入命令,%prompt%, , 300,200
    if ErrorLevel
        return
    if (cmd_Buff="")
        return
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
    ActiveSheet:=excel.ActiveSheet
try{
    if RegExMatch(cmd_Buff,"i)[a-z]+(\d)+")
    {
        buff_string := substr(cmd_buff, 1)
        excel.ActiveSheet.range(buff_string).Activate
    } else if RegExMatch(cmd_Buff,"im)[a-z]+$") {
        buff_string := substr(cmd_buff, 1)
        column_string := buff_string
        RngAdd:=column_string . excel.ActiveCell.Row
        excel.ActiveSheet.range(RngAdd).Activate
    } else if RegExMatch(cmd_Buff,"#*") {
        buff_string := substr(cmd_buff, 1)
        row_number := buff_string
        excel.Cells(row_number, excel.ActiveCell.Column).Activate
    } 
    excel.ScreenUpdating := True
}

Return
F16529BB-FCA0-4241-833F-3F121076BFAB:
;Phantom_切换到【普通模式】
send,{esc}
vimd.changemode("普通模式")
MsgBox, 0, 提示, 【普通模式】, 0.3
Return
F282582F-9E5C-48B7-B74A-A31C3F71982B:
;Excel_编辑单元格（F2功能）
sendinput,{F2}
Return
F2832E84-0F6E-460C-A572-7E2B283234A2:
;Phantom_编辑-输入标签内容
global CurID
global PageStr:=[]

gui,font,s12
Gui Add, Edit, x9 y2 w465 h400 vMyVar_PDFLabels,
Gui Add, Button, x78 y405 w75 h32 default gMySub_PDFLabelOK, 确认
Gui Add, Button, x250 y405 w75 h32 gMySub_PDFLabelClear, 清空
gui,font
Gui Show, w481 h440, 输入标签内容
Return

MySub_PDFLabelOK()
{
	global CurID
	global PageStr:=[]
	GuiControlget,MyVar_PDFLabels,,MyVar_PDFLabels
	Loop, parse, MyVar_PDFLabels, `n, `r  ; 
	{
		if (A_LoopField!="")
			PageStr[A_Index]:=A_LoopField
	}
	CurID:=1
	gui,destroy
	msgbox,0,,% "已加载" PageStr.Length() "个书签",1
	return
}

MySub_PDFLabelClear()
{
	GuiControl,,MyVar_PDFLabels
	return
}
Return
F462451E-AFCB-47BB-ACB1-CD54409EC7AE:
;Excel_错误检查
;try
;comObjactive("excel.application").run("Personal.xlsb!错误检查")

excel:=Excel_Get()
ActiveCell:=excel.ActiveCell
ActiveSheet:=excel.ActiveSheet
loop,% excel.Worksheets.Count
{
	Esh:=excel.Worksheets(A_index)
	Esh.activate
	loop,% Esh.UsedRange.count
	{
		ERng:=Esh.UsedRange.cells(A_index)
		
		if (ERng.text="#DIV/0!")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#DIV/0!"
			ERng.Select
			return
		}
		if (ERng.text="#N/A")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#N/A"
			ERng.Select
			return
		}
		if (ERng.text="#NAME?")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#NAME?"
			ERng.Select
			return
		}
		if (ERng.text="#NULL!")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#NULL!"
			ERng.Select
			return
		}
		if (ERng.text="#NUM!")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#NUM!"
			ERng.Select
			return
		}
		if (ERng.text="#REF!")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#REF!"
			ERng.Select
			return
		}
		if (ERng.text="#VALUE!")
		{
			MsgBox % "工作表：" . Esh.Name . "的单元格：" . ERng.Address . "`n" . "错误类型：#VALUE!"
			ERng.Select
			return
		}
	}	
}
MsgBox, 64, Excel_VIMD, 检查完毕，未发现错误。

Return
F4D30BA8-D713-4E16-A69A-FC56BB75C978:
;QQ浏览器_下一个标签
sendinput,^{tab}
Return
F505D9F9-E882-45DF-AC5A-B2A520CB4E7E:
;Phantom_文件-保存关闭
sendinput,^s
sendinput,^w
Return
F5147277-BA3F-445B-9046-92471EC77D9B:
;通用_向下
sendinput,{down}
Return
F5181FE4-35F2-419A-9532-531A7733E8AB:
;Excel_显示/不显示【零值】
	excel:=Excel_Get()
try
	excel.ActiveWindow.DisplayZeros := not excel.ActiveWindow.DisplayZeros
Return
F558849D-49C2-4785-B63E-D4F5406529E2:
;通用_Home
sendinput,{Home}
Return
F6488CB0-D842-4332-B250-05B0733FD5E7:
;Excel_筛选-与所选单元格前1-7个字符相同
    buff_string:="f7"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
F6B5A97D-833A-4A9A-80E6-56914B7654B7:
;Excel_小数点3位
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0.000"
Return
F8F14610-C9A8-4A92-AF28-4CF4F0A95577:
;通用_VIMD清除输入
QZ_VIMD_ClearInput()
Return
FA31A1AC-0DD5-4A4D-8FD3-E77BA22DCC24:
;Excel_筛选-与所选单元格前1-6个字符相同
    buff_string:="f6"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", substr(ActiveCell.Value,1,begin_int)  "*",buff_string)
Return
FAF17C59-C8C4-4F1E-9744-35347F2FFB09:
;Excel_选中列
    excel:=Excel_Get()
    Selection:=excel.Selection
    ;Rngs_left := Selection.Column
    ;Rngs_top := Selection.Row
    ;Rngs_right := Rngs_left + Selection.Columns.Count - 1
    ;Rngs_bottom := Rngs_top + Selection.Rows.Count - 1
    ;leftC := StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[2]
    ;rightC := StrSplit(excel.Cells(Rngs_top, Rngs_right).Address, "$")[2]
    ;topR:=StrSplit(excel.Cells(Rngs_top, Rngs_left).Address, "$")[3]
    ;bottomR:=StrSplit(excel.Cells(Rngs_bottom, Rngs_left).Address, "$")[3]
    ;Selected_columns_string:=leftC . ":" . rightC
    ;Selected_range :=  excel.ActiveSheet.Range(Selected_columns_string)
    ;Selected_range.select
try
    Selection.EntireColumn.Select
Return
FB54BCF4-A4BD-429C-A91D-0A8BF1F78A94:
;&5  重命名 (父文件夹-)文件、文件夹名
MySel:=QZData("files")
ParentDir:=
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	if (A_index=1)
	{
		SplitPath,MyTemFile,MyOutFileName,MyOutDir
		SplitPath,MyOutDir,ParentDir
	}
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	if InStr(FileExist(MyTemFile), "D")
	{
		MyNewFile =%MyOutDir%\%ParentDir%-%MyOutNameNoExt%
		FileMoveDir,%MyTemFile%,%MyNewFile%
	} else {
		MyNewFile =%MyOutDir%\%ParentDir%-%MyOutNameNoExt%.%MyOutExt%
		FileMove,%MyTemFile%,%MyNewFile%
	}
}
Return
FB7CC282-E735-4485-A2FC-D7AE368F70AB:
;&4  重命名 文件、文件夹名(输入内容)
InputBox, MyInsert, 提示,请输入要添加的内容
if (MyInsert="")
	return
MySel:=QZData("files")
loop,Parse,MySel,`n     ;循环读取每一行
{
	MyTemFile := RegExReplace(A_loopfield,"(.*)\r.*","$1")
	SplitPath,MyTemFile,MyOutFileName,MyOutDir,MyOutExt,MyOutNameNoExt,MyOutDrive
	if InStr(FileExist(MyTemFile), "D")
	{
		MyNewFile =%MyOutDir%\%MyOutNameNoExt%%MyInsert%
		FileMoveDir,%MyTemFile%,%MyNewFile%
	} else {
		MyNewFile =%MyOutDir%\%MyOutNameNoExt%%MyInsert%.%MyOutExt%
		FileMove,%MyTemFile%,%MyNewFile%
	}
}
Return
FC8533D2-7C2E-4829-9493-C8831BFEA2F9:
;Excel_选中列-隐藏
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.EntireColumn.Hidden := True
Return
FCA707B5-8746-4E2E-AA39-F3E094D0000C:
;Excel_重置所选列筛选
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("", ActiveCell.Value,"fa")
Return
FDB75501-6D2A-4253-8B01-2951D4252347:
;Excel_设置/取消粗体
    excel:=Excel_Get()
    Selection:=excel.Selection
try{
    If (Selection.Font.Bold)
        Selection.Font.Bold := False
    Else
        Selection.Font.Bold := True
}
Return
FE5BF3B2-DD07-4A8B-988D-D4157EE2C1C8:
;Excel_筛选-与所选单元格后1-5个字符相同
    buff_string:="F5"
    begin_int:=
    begin_int :=substr(buff_string, 0)
    excel:=Excel_Get()
    ActiveCell:=excel.ActiveCell
try
    CustomAutoFilter("=", "*" substr(ActiveCell.Value,1-begin_int),buff_string)
Return
FFD8A23C-27AA-461F-AC25-26F316DFB270:
;Excel_用括号格式化负数
    excel:=Excel_Get()
    Selection:=excel.Selection
try
    Selection.NumberFormatLocal := "0_);[红色](0)"
Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
FSViewer_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
FSViewer_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
FSViewer_ActionBefore()
{
    Global VimD
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
FSViewer_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
FSViewer_Show(aTemp, aMore)
{
    Global VimD
}

Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
QQBrowser_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
QQBrowser_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
QQBrowser_ActionBefore()
{
    Global VimD
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
QQBrowser_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
QQBrowser_Show(aTemp, aMore)
{
    Global VimD
}

Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
Global_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
Global_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
Global_ActionBefore()
{
    Global VimD
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
Global_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
Global_Show(aTemp, aMore)
{
    Global VimD
}

Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
TOTALCMD64_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
TOTALCMD64_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
TOTALCMD64_ActionBefore()
{
    Global VimD
    ControlGetFocus, ctrl, ahk_class TTOTAL_CMD
    If RegExMatch(ctrl, "i)Edit")    ;查找时
        Return True
    if !Instr(ctrl,"TMyListBox")
        Return True
    return false
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
TOTALCMD64_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
TOTALCMD64_Show(aTemp, aMore)
{
    Global VimD
}

Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
Foxit_Phantom_Init()
{
   Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
Foxit_Phantom_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
Foxit_Phantom_ActionBefore()
{
    Global VimD
    ;以下代码实现在查找内容时，自动跳转到普通模式
    ControlGetFocus, ctrl, A
    If RegExMatch(ctrl, "i)Edit")    ;查找时
        Return True
    return False
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
Foxit_Phantom_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
Foxit_Phantom_Show(aTemp, aMore)
{
    Global VimD
}

Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
ExcelAll_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
ExcelAll_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
ExcelAll_ActionBefore()
{
    Global VimD

   ;以下代码实现仅在指定区域内里使用VIM模式
     ;excel71,		;单元格区域
     ;netuihwnd2,		;选项卡
     ;netuihwnd4		;垂直滚动条
     ;netuihwnd5		;水平滚动条
    ControlGetFocus, ctrl, A
    If RegExMatch(ctrl, "i)excel71") || RegExMatch(ctrl, "i)netuihwnd2") || RegExMatch(ctrl, "i)netuihwnd4") || RegExMatch(ctrl, "i)netuihwnd5")
        Return false
    return True
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
ExcelAll_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
ExcelAll_Show(aTemp, aMore)
{
    Global VimD
}

Return
