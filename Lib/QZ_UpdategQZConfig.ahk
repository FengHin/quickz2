﻿/*
    Function: QZ_UpdateCode()
        保存Items中的代码到 Lib\QZ_Codes.ahk
*/
QZ_UpdateCategory(OriCategory,NewCategory)
{
    Global  gQZConfig
    for k,v in gQZConfig.items
    {
        if(v.options.category=OriCategory)
            gQZConfig.items[k].options.category:=NewCategory
    }
}

QZ_UpdateFilterMode(OriMode,NewMode)
{
    Global  gQZConfig
    loop, % gQZConfig.menuz.MaxIndex()
    {
        objMenu:=[]
        objMenu:=gQZConfig.menuz[A_Index]
        gQZConfig.menuz[A_Index]:=QZ_UpdateSubMode(objMenu,OriMode,NewMode)
    }
    for k,v in gQZConfig.hotkeys
    {
        if(v.filtermode=OriMode)
            gQZConfig.hotkeys[k].filtermode:=NewMode
    }
	;~ QZ_WriteConfig(gQZConfig,QZGlobal.Config)
    ;~ Send_WM_CopyData("reload")
}

QZ_UpdateSubMode(aobj,OriMode,NewMode)
{
    ObjArray:=[]
    ObjArray:=aobj
    if (ObjArray.SubItem.Length()>0)
    {
        Loop,% ObjArray.subitem.MaxIndex()
        {
            bobj:=[]
            bobj:=ObjArray.subitem[A_index]
            bobj:=QZ_UpdateSubMode(bobj,OriMode,NewMode)
            ObjArray.subitem[A_index]:=bobj
        }
    }
    else
    {
        TemFilterModes:=""
        TemFilterModes:=ObjArray.filtermodes
        TemFilterModes:=RegExReplace(TemFilterModes,OriMode,NewMode,all)
        TemFilterModes:=RegExReplace(TemFilterModes,";{2,}",";",all)
        TemFilterModes:=RegExReplace(TemFilterModes,";$","",all)
        ObjArray.filtermodes:=TemFilterModes
    }
    return ObjArray
}
