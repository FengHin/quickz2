﻿/*
Filter=CustomFilter_NotFilePath
Tips=仅非文件路径型文本
*/

CustomFilter_NotFilePath(){
	return !RegExMatch(QZData("Text"),"^[a-zA-Z]:\\.*")
}