﻿/*
API=CustomFunc_QZSel
Tips=获取数据，可兼容热键
Author=Array、Kawvin、LL
Version=0.3
*/

/*
    函数: CustomFunc_QZSel
        此 API 用来获取选中的数据，兼容菜单和热键。

    参数:
        aParam - 无需参数

    返回:
        必然返回一个全局的 QZSel 对象，同时以赋值方式返回到用户指定的对象。

    作者: Array、Kawvin、LL

    版本: 0.3
*/
CustomFunc_QZSel(strMatch:="") {
    global QZSel
    QZSel:={}
    VimdArray:={}
    ExeArray:={}
    
    CustomFunc_QZ_GetClip(VimdArray)
    QZ_GetWinInfo(ExeArray)
    QZSel.files:=StrLen(VimdArray.files VimdArray.text)?VimdArray.files:QZData("files")
    QZSel.FileExt:=StrLen(VimdArray.files VimdArray.text)?VimdArray.FileExt:QZData("FileExt")
    QZSel.FileDir:=StrLen(VimdArray.files VimdArray.text)?VimdArray.FileDir:QZData("FileDir")
    QZSel.FileName:=StrLen(VimdArray.files VimdArray.text)?VimdArray.FileName:QZData("FileName")
    QZSel.text:=StrLen(VimdArray.files VimdArray.text)?VimdArray.text:QZData("text")
    QZSel.x:=ExeArray.x
    QZSel.y:=ExeArray.y
    QZSel.HWND:=ExeArray.HWND
    if not QZSel.HWND
    {
        WinGet, iHwnd, ID, A
        QZSel.HWND:=iHwnd
    }
    QZSel.WinClass:=ExeArray.WinClass
    QZSel.WinExe:=ExeArray.WinExe
    QZSel.WinControl:=ExeArray.WinControl
    QZSel.WinTitle:=ExeArray.WinTitle
    
    QZSel.ByHotkey:=StrLen(VimdArray.files VimdArray.text)?1:0 ;记录最后一次获取数据是用热键触发还是用菜单触发
    QZSel.ByMenu:=QZSel.ByHotkey?0:1
    
    if strMatch
        QZSel.files:=CustomFunc_QZSelParse(QZSel.files,strMatch)
    
    return QZSel
}

CustomFunc_QZSelParse(objFile,strMatch:="")
{
    objFile := new QZ_LabelFile(objFile)
    return objFile.Parse(strMatch)
}