﻿CustomFunc_uXXXX2CN(uXXXX)
{	; by 卢霖
	Loop, Parse, uXXXX, u, \
	{
		if StrLen(A_LoopField)<4
		{
			MatchStr:=A_LoopField
			c=0
			UnicodeTable:=UnicodeTable()
			UnicodeTable:=RegExReplace(UnicodeTable, "[\r\n]")
			StringReplace, UnicodeTable ,UnicodeTable,`n\u,￠, All
			Loop, Parse, UnicodeTable,￠
			{
				if A_LoopField
				{
					if MatchStr=%A_LoopField%
					{
						retStr .= RegExReplace(A_LoopField, "\w{1,4} ")
						break
					}
				}
				else
				{
					if c
						break
					continue
				}
			}
		}
		retStr .= Chr("0x" . A_LoopField) ;为字符串添加16进制前缀。字符=Chr(编码)。
	}
	return retStr
}

uXXXX2CN_Original(uXXXX) ;经测试在 L 版下运行最快
{	; by RobertL
	Loop, Parse, uXXXX, u, \
		retStr .= Chr("0x" . A_LoopField) ;为字符串添加16进制前缀。字符=Chr(编码)。
	return retStr
}

CN2uXXXX(cnStr) ;经测试在 L 版下运行最快
{ ; from tmplinshi
	OldFormat := A_FormatInteger
	SetFormat, Integer, Hex
	Loop, Parse, cnStr
		out .= "\u" . SubStr( Asc(A_LoopField), 3 )
	SetFormat, Integer, %OldFormat%
	Return out
}


uXXXX2CN(uXXXX) ; 最开始的版本，只有 uXXXX 到文本，效率不高，而且不支持单字节字符，唯一优点就是可以适用于原版, L/H 版。
{
	StringReplace, uXXXX, uXXXX, \u, #, A
	cCount := StrLen(uXXXX) / 5
	VarSetCapacity(UUU, cCount * 2, 0)
	cCount := 0
	loop, parse, uXXXX, #
	{
		if ( "" = A_LoopField )
			continue
		NumPut("0x" . A_LoopField, &UUU+0, cCount)
		cCount += 2
	}
	if ( A_IsUnicode ) {
		return, UUU
	} else {
		CustomFunc_Unicode2Ansi(UUU, rUUU, 0)
		return, rUUU
	}
}

CustomFunc_Unicode2Ansi(ByRef wString, ByRef sString, CP = 0)  ; 这个函数是从论坛上抄下来的
{
	nSize := DllCall("WideCharToMultiByte", "Uint", CP, "Uint", 0, "Uint", &wString, "int",  -1, "Uint", 0, "int", 0, "Uint", 0, "Uint", 0) 
	VarSetCapacity(sString, nSize)
	DllCall("WideCharToMultiByte", "Uint", CP, "Uint", 0, "Uint", &wString, "int",  -1, "str",  sString, "int",  nSize, "Uint", 0, "Uint", 0)
}

UnicodeTable() {
UnicodeTable=
(
\u30 0
\u31 1
\u32 2
\u33 3
\u34 4
\u35 5
\u36 6
\u37 7
\u38 8
\u39 9
\u61 a
\u62 b
\u63 c
\u64 d
\u65 e
\u66 f
\u67 g
\u68 h
\u69 i
\u6A j
\u6B k
\u6C l
\u6D m
\u6E n
\u6F o
\u70 p
\u71 q
\u72 r
\u73 s
\u74 t
\u75 u
\u76 v
\u77 w
\u78 x
\u79 y
\u7A z
\u41 A
\u42 B
\u43 C
\u44 D
\u45 E
\u46 F
\u47 G
\u48 H
\u49 I
\u4A J
\u4B K
\u4C L
\u4D M
\u4E N
\u4F O
\u50 P
\u51 Q
\u52 R
\u53 S
\u54 T
\u55 U
\u56 V
\u57 W
\u58 X
\u59 Y
\u5A Z
\u2C ,
\u2E .
\u21 !
\u3F ?
\u7E ~
\u60 ``
\u40 @
\u23 #
\u24 $
\u25 `%
\u5E ^
\u26 &
\u2A *
\u28 (
\u29 )
\u5F _
\u2B +
\u7B {
\u5B [
\u7D }
\u5D ]
\u3A :
\u3B ;
\u22 "
\u27 '
\u7C |
\u5C \
\u3C <
\u3E >
\u2F /
) ;延续片段已存到变量 UnicodeTable 中
return UnicodeTable
}