﻿/*
Label={box:filebrowser}
Tips=打开文件选择对话框
Author=Kawvin
Version=1.0
*/

box_filebrowser(aParam:="")
{
	FileSelectFile, f_File ,,, 请选择文件
	if ErrorLevel
		return
	StringReplace,aParam,aParam,{box:Filebrowser},%f_File%,All
    ;msgbox % aParam "OK"
	return aParam
}