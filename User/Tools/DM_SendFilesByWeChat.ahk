﻿#Include %A_ScriptDir%\Lib\WinClipAPI.ahk
#Include %A_ScriptDir%\Lib\WinClip.ahk
/*
Plugin=DM_SendFilesByWeChat
Name1=通过微信发送文件
Command1=DM_SendFilesByWeChat
Author=Kawvin
Version=0.1版本
说明：批量添加，由|分开,=为分隔符,如：张三|李四||王五
*/

DM_SendFilesByWeChat(aParam){    ; 此Plugin执行的command为Test，代表执行Test函数,函数请预留一个aParam，用于QuickZ传递参数过来。
	icon:=A_ScriptDir . "\User\Icons\WeChat.ico"
    ;Menu_SendFilesByQQ := MenuZ_GetSibling() ; 获取一个菜单对象，不是子菜单，是同级菜单
	Menu_SendFilesByWeChat := MenuZ_GetSub() ; 获取一个子菜单对象
	Menu_SendFilesByWeChat.SetParams({iconsSize:24})
	IndexArray:=["1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	Menu_SendFilesByWeChat.Add({name:"&1  发送到  文件传输助手",icon: icon,uid:{Handle:"DM_SendFilesByWeChat_Handle",Data:"文件传输助手"}})
	Menu_SendFilesByWeChat.Add()
	if !strlen(aParam)
		Menu_SendFilesByWeChat.Add({name:"批量添加，由|分开,||为分隔符,如：张三|李四||王五",icon: icon,uid:{Handle:"DM_SendFilesByWeChat_Handle",Data:""}}) 
	Index:=2
	loop,parse, aParam,`|
	{
		MenuString:=trim(A_LoopField)
		if (MenuString!="") 
		{
			Menu_SendFilesByWeChat.Add({name:"&" Index "  发送到  " MenuString,icon: icon,uid:{Handle:"DM_SendFilesByWeChat_Handle",Data:MenuString}}) ; Handle的值为点击菜单后执行的功能
			Index+=1
		}
		else
			Menu_SendFilesByWeChat.Add()
	}
    return Menu_SendFilesByWeChat  ; 必须返回子菜单对象
}

DM_SendFilesByWeChat_Handle(aMsg, aObj)
{
    If (aMsg = "OnRun")
		Menu_SendFilesByWeChat_Action(aObj.Uid.Data)
	;~ else If (aMsg = "OnRButton")			
	;~ else If (aMsg = "onmbutton")
}

Menu_SendFilesByWeChat_Action(aMsg)
{
	if (aMsg="")
		return
	;~ #WinActivateForce
	;~ Curhwnd:=QZData("hWnd")
	;~ WinActivate,Ahk_ID %Curhwnd%
	;~ sleep,100
	;~ PreClipboard :=ClipboardAll
	;~ Clipboard:=
	;~ Send, ^{vk43}
	;~ ClipWait,2
	;~ if ( ErrorLevel  )          ;如果没有选择到什么东西，则退出
	;~ {
		;~ MsgBox, 48, 提示, 获取文件失败！
		;~ Return
	;~ }
	MySel:=QZData("files")
	MyAppPath:=QZData("%WeChat%")
	try
		run,%MyAppPath%
	WinWaitActive, 微信 ,,10
	if ErrorLevel
	{
		MsgBox, 64, 提示, 打开微信失败
		return
	}
	WinActivate,ahk_class WeChatMainWndForPC
	;ControlSetText,EditWnd1,% MyFun_uStr1(aMsg),ahk_class WeChatMainWndForPC
	ControlClick, x90 y33, 微信,,,, Pos	;相对于窗体左上角的相对位置点击
	;删除已有数据
	sendinput,^a
	sendinput,{del}
	sleep,50
	Clipboard:=aMsg
	sleep,50
	send,^{vk56}	;粘贴
	Clipboard=
	;~ if (aMsg="文件传输助手")
	;~ {
		;~ sendinput , % MyFun_getAscStrW("文件传输助手")
	;~ } else {
		;~ sendinput , % MyFun_getAscStrW(aMsg)
	;~ }
	sleep,400
	Send,{Enter}
	sleep,200
	;WinWaitActive, 微信 ,,5
	WinActivate, 微信
	WinWaitActive, 微信 ,,5
	;sleep,200
	loop,parse,MySel,`n,`r
	{
		WinClip.AppendFiles(A_LoopField)		;通过WinClip类将文件置入剪贴板
		sleep,100
	}
	send,^{vk56}	;粘贴文件
	Clipboard=
	sleep,1000
	sendinput,^{enter}
}

MyFun_uStrW(str)
{
	charList:=StrSplit(str)
	SetFormat, integer, hex
	for key,val in charList
		out.="{U+ " . ord(val) . "}"
	return out
}

MyFun_getAscStrW(str)
{
    charList:=StrSplit(str)
    for key,val in charList
        out.="{Asc " . asc(val) . "}"
    return out
}
